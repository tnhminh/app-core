/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.jcl;

import com.adsplay.common.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xeustechnologies.jcl.ClasspathResources;
import org.xeustechnologies.jcl.JarClassLoader;
import org.xeustechnologies.jcl.exception.JclException;

import java.io.*;
import java.util.Collection;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * @author khoivu
 */
public class JarLoader extends JarClassLoader {

    private final transient Logger logger = LoggerFactory.getLogger(ClasspathResources.class);

    public JarLoader() {
    }

    public JarLoader(Collection sources) {
        super(sources.toArray());
    }

    public void reloadJar(String jarFile) {
        logger.debug("Reload jar: {}", jarFile);
        FileInputStream fis = null;
        try {
            File file = new File(jarFile);
            String baseUrl = "jar:" + file.toURI().toString() + "!/";
            fis = new FileInputStream(file);
            unloadJar(baseUrl, fis);
            classpathResources.loadJar(baseUrl, fis);
        } catch (IOException e) {
            throw new JclException(e);
        } finally {
            Utils.close(fis);
        }
    }

    public void unloadJar(String argBaseUrl, InputStream jarStream) {
        BufferedInputStream bis = null;
        JarInputStream jis = null;
        try {
            bis = new BufferedInputStream(jarStream);
            jis = new JarInputStream(bis);

            JarEntry jarEntry;
            while ((jarEntry = jis.getNextJarEntry()) != null) {
                logger.debug(dump(jarEntry));

                if (jarEntry.isDirectory()) {
                    continue;
                }
                classpathResources.unload(jarEntry.getName());
                logger.debug("Entry Name: {}, Entry Size: {}", jarEntry.getName(), jarEntry.getSize());
            }
        } catch (IOException e) {
            throw new JclException(e);
        } catch (NullPointerException e) {
            logger.debug("Done reload.");
        } finally {
            Utils.close(jis);
            Utils.close(bis);

        }
    }

    private String dump(JarEntry je) {
        StringBuilder sb = new StringBuilder();
        if (je.isDirectory()) {
            sb.append("d ");
        } else {
            sb.append("f ");
        }

        if (je.getMethod() == JarEntry.STORED) {
            sb.append("stored   ");
        } else {
            sb.append("defalted ");
        }

        sb.append(je.getName());
        sb.append("\t");
        sb.append(je.getSize());
        if (je.getMethod() == JarEntry.DEFLATED) {
            sb.append("/").append(je.getCompressedSize());
        }

        return (sb.toString());
    }

    @Override
    public void add(String resourceName) {
        super.add(resourceName);
        File file = new File(resourceName);
        String[] libraries = getLibraries(file);
        if (libraries == null || libraries.length == 0) {
            return;
        }
        File currentDir = file.getParentFile();
        logger.debug("Load library of [{}] Current dir: {}", file.getName(), currentDir.getAbsoluteFile());
        for (String library : libraries) {
            if (Utils.isEmpty(libraries)) {
                continue;
            }
            File libFile = new File(currentDir, library);
            if (libFile.exists()) {
                logger.debug("Load library of [{}]", libFile.getAbsoluteFile());
                add(libFile.getAbsolutePath());
            }
        }
    }

    private String[] getLibraries(File file) {
        if (file.isFile() & file.getName().toLowerCase().endsWith(".jar")) {
            BufferedInputStream bis = null;
            JarInputStream jis = null;
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                jis = new JarInputStream(bis);
                Attributes attributes = jis.getManifest().getMainAttributes();
                String values = attributes.getValue("Class-Path");
                if (Utils.isEmpty(values)) {
                    return null;
                }
                return values.split(" ");
            } catch (IOException ex) {
                logger.error("Error when Load library of [{}]", file.getAbsoluteFile(), ex);
            } finally {
                Utils.close(jis);
                Utils.close(bis);
                Utils.close(fis);
            }
        }
        return null;
    }

}
