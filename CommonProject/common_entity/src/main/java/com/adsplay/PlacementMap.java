package com.adsplay;

/**
 * User: khoivu
 * Date: 5/9/18
 * Time: 5:07 PM
 */
public enum PlacementMap {

    WEB                             (102, "web", "", "video"),
    SMART_TV_WEB                    (202, "smart_tv", "web", "video"),
    SMART_TV_ANDROID                (308, "smart_tv", "android", "video"),
    APP_IOS                         (302, "app", "ios", "video"),
    APP_ANDROID                     (306, "app", "android", "video"),
    LIVE_TV                         (600, "", "", "livetv"),
    OTHER                           (-1, "", "", "")
    ;

    private final int placementId;
    private final String type;
    private final String appType;
    private final String deliveryType;

    PlacementMap(int placementId, String type, String appType, String deliveryType) {
        this.placementId = placementId;
        this.type = type;
        this.appType = appType;
        this.deliveryType = deliveryType;
    }


    public int getPlacementId() {
        return placementId;
    }

    public String getType() {
        return type;
    }

    public String getAppType() {
        return appType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public static PlacementMap findByCode(int placementId) {
        for (PlacementMap map : values()) {
            if (map.getPlacementId() == placementId) {
                return map;
            }
        }
        return PlacementMap.OTHER;
    }

}
