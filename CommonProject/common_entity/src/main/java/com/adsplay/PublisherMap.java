package com.adsplay;

/**
 * User: khoivu
 * Date: 5/8/18
 * Time: 11:16 AM
 */
public enum PublisherMap {

    PAY_TV              (320, 2, "Pay TV"),
    FPT_PLAY            (308, 1, "FPT Play"),
    WIFI_HOTSPOT        (1000, 22, "FPT Telecom"),
    MEGANET_WIFI        (1001, 30, "Meganet"),
    OTHER               (350, 0, "OTHER")
    ;

    private final int placementId;
    private final int publisherId;
    private final String name;

    PublisherMap(int placementId, int publisherId, String name) {
        this.placementId = placementId;
        this.publisherId = publisherId;
        this.name = name;
    }

    public int getPlacementId() {
        return placementId;
    }

    public int getPublisherId() {
        return publisherId;
    }

    public String getName() {
        return name;
    }

    public static String findNameByPublisherId(int publisherId) {
        for (PublisherMap publisher : values()) {
            if (publisher.getPublisherId() == publisherId) {
                return publisher.getName();
            }
        }
        return PublisherMap.OTHER.getName();
    }

}
