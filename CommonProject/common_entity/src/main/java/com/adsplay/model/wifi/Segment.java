package com.adsplay.model.wifi;

public class Segment {

    private Integer id = -1;
    private String name = "-";
    private String key = "-";

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }
}
