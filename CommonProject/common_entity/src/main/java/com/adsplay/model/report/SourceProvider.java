package com.adsplay.model.report;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SourceProvider {
    private long id;
    private String name;
    @JsonProperty("account_id")
    private long accountId;
    @JsonProperty("publisher_code")
    private String publisherCode;
    private long activated;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getAccountId() {
        return accountId;
    }

    public String getPublisherCode() {
        return publisherCode;
    }

    public long getActivated() {
        return activated;
    }
}
