package com.adsplay.model;

/**
 * User: khoivu
 * Date: 5/7/18
 * Time: 3:00 PM
 */
public class UserAgentModel {

    private String deviceAppType;
    private String deviceName;
    private String deviceOsVersion;
    private String brandName;
    private String userAgent;
    private String osVersionBuild;

    public String getOsVersionBuild() {
        return osVersionBuild;
    }

    public void setOsVersionBuild(String osVersionBuild) {
        this.osVersionBuild = osVersionBuild;
    }

    public String getDeviceAppType() {
        return deviceAppType;
    }

    public void setDeviceAppType(String deviceAppType) {
        this.deviceAppType = deviceAppType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
