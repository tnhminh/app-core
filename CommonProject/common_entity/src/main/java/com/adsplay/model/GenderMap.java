package com.adsplay.model;

/**
 * User: khoivu
 * Date: 10/5/18
 * Time: 10:45 AM
 */
public enum GenderMap {
    MALE                                         (1, "Male"),
    FEMALE                                       (0, "Female"),
    OTHER                                        (-1,"Other"),
    ;

    private final int id;
    private final String value;

    GenderMap(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public static GenderMap findByCode(int id) {
        for (GenderMap gender : values()) {
            if (gender.getId() == id) {
                return gender;
            }
        }
        return GenderMap.OTHER;
    }

}
