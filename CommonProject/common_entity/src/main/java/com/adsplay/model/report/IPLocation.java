package com.adsplay.model.report;

/**
 * User: khoivu
 * Date: 5/9/18
 * Time: 10:30 AM
 */

public class IPLocation {

    private String country;
    private String provinceName;
    private String provinceId;

    public String getCountry() {
        return country;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }
}
