package com.adsplay.model.report;

/**
 * User: khoivu
 * Date: 5/9/18
 * Time: 10:48 AM
 */

public class DateTimeReport {

    private long time;
    private String date;
    private String hour;
    private String dateTime;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
