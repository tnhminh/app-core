package com.adsplay.model.wifi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WifiLocation {
    @JsonProperty("id")
    private Integer id = -1;
    @JsonProperty("ap_id")
    private Integer apId = -1;
    @JsonProperty("mac_address")
    private String macAddress = "-";
    @JsonProperty("access_point")
    private AccessPoint accessPoint = new AccessPoint();
    @JsonProperty("zone")
    private Zone zone = new Zone();
    @JsonProperty("place")
    private Place place = new Place();

    public Integer getId() {
        return id;
    }

    public Integer getApId() {
        return apId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public AccessPoint getAccessPoint() {
        return accessPoint;
    }

    public Zone getZone() {
        return zone;
    }

    public Place getPlace() {
        return place;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setApId(Integer apId) {
        this.apId = apId;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public void setAccessPoint(AccessPoint accessPoint) {
        this.accessPoint = accessPoint;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}
