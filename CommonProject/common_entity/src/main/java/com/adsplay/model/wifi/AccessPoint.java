package com.adsplay.model.wifi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccessPoint {

    private Integer id = -1;
    private String name = "-";
    @JsonProperty("zone_id")
    private Integer zoneId = -1;
    @JsonProperty("mac_address")
    private String macAddress = "-";
    @JsonProperty("mac_wifi_start")
    private String macWifiStart = "-";
    @JsonProperty("mac_wifi_end")
    private String macWifiEnd = "-";
    @JsonProperty("ip_address")
    private String ipAddress = "-";
    private String model = "-";
    private String tag = "-";
    private String serial = "-";
    @JsonProperty("os_version")
    private String osVersion = "-";

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getZoneId() {
        return zoneId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public String getMacWifiStart() {
        return macWifiStart;
    }

    public String getMacWifiEnd() {
        return macWifiEnd;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getModel() {
        return model;
    }

    public String getTag() {
        return tag;
    }

    public String getSerial() {
        return serial;
    }

    public String getOsVersion() {
        return osVersion;
    }
}
