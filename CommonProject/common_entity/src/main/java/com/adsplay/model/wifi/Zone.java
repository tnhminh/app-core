package com.adsplay.model.wifi;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Zone {

    @JsonProperty("id")
    private Integer id = -1;
    @JsonProperty("name")
    private String name = "-";
    @JsonProperty("place_id")
    private Integer placeId = -1;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPlaceId() {
        return placeId;
    }
}
