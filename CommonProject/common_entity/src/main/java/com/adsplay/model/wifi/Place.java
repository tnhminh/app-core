package com.adsplay.model.wifi;

import com.adsplay.model.report.SourceProvider;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Place {

    private Integer id = -1;
    private String name = "-";
    @JsonProperty("sp_id")
    private Integer spId = -1;
    @JsonProperty("segment_key")
    private String segmentKey = "-";
    private String province = "-";
    private String region = "-";
    private String district = "-";
    private String ward = "-";
    private Segment segment = new Segment();
    @JsonProperty("source_provider")
    private List<SourceProvider> sourceProvider = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getSpId() {
        return spId;
    }

    public String getSegmentKey() {
        return segmentKey;
    }

    public String getProvince() {
        return province;
    }

    public String getRegion() {
        return region;
    }

    public String getDistrict() {
        return district;
    }

    public String getWard() {
        return ward;
    }

    public Segment getSegment() {
        return segment;
    }

    public List<SourceProvider> getSourceProvider() {
        return sourceProvider;
    }
}
