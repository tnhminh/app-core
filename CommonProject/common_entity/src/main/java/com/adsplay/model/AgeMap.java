package com.adsplay.model;

/**
 * User: khoivu
 * Date: 10/10/18
 * Time: 10:31 AM
 */
public enum AgeMap {
    UNDER_18                                     (-1,17, "Under 18"),
    AGE_18_24                                    (18,24, "18-24"),
    AGE_25_34                                    (25,34, "25-34"),
    AGE_35_44                                    (35,44, "35-44"),
    AGE_45_54                                    (45,54, "45-54"),
    AGE_55_64                                    (55,64, "55-64"),
    About_65                                     (65,200, "About 65"),
    OTHER                                        (-1,-1,"Other"),
    ;

    private final int from;
    private final int to;
    private final String value;

    AgeMap(int from, int to, String value) {
        this.from = from;
        this.to = to;
        this.value = value;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public String getValue() {
        return value;
    }

    public static AgeMap findByValue(String value) {
        for (AgeMap ageMap : values()) {
            if (ageMap.getValue() == value) {
                return ageMap;
            }
        }
        return AgeMap.OTHER;
    }

    public static String findByAge(int age) {
        for (AgeMap ageMap : values()) {
            if (ageMap.getFrom() <= age && age <= ageMap.getTo()) {
                return ageMap.getValue();
            }
        }
        return AgeMap.OTHER.getValue();
    }
}
