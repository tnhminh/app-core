package com.adsplay;

/**
 * User: khoivu
 * Date: 9/27/18
 * Time: 1:56 PM
 */
public class Constant {

    //Sentry tags key
    public static final String SENTRY_BREADCRUMB_MESSAGE = "No action";

    public static final String SENTRY_BREADCRUMB_CATEGORY = "No category";

    public static final String SENTRY_TAG_MESSAGE = "message";

    public static final String LOG_REQUEST_ID = "requestId";

    public static final String LOG_RECEIVE_DATA = "receiveData";

    public static final String LOG_PROCESS_NAME = "processName";

    public static final String AUTHORIZATION = "Authorization";

    public static final String MAC = "mac";

    public static final String USER_AGENT = "User-Agent";

    public static final String TYPE = "type";

    public static final String FROM_DATE = "from-date";

    public static final String TO_DATE = "to-date";

    public static final String COLON = ":";

    public static final String ANY_SPACE = "\\s+";

    public static final String COMMA = ",";

    public static final String WEIGHT_PREFIX = "w_";

    public static final String DASH = "_";

    public static final String USER = "user";

    public static final String PASS = "pass";

    public static final String ROLE = "role";

    //UserAgent FieldNames
    public static final String AGENT_DEVICE_CLASS = "DeviceClass";

    public static final String AGENT_DEVICE_NAME = "DeviceName";

    public static final String AGENT_DEVICE_BRAND = "DeviceBrand";

    public static final String AGENT_DEVICE_VERSION = "OperatingSystemVersion";

    public static final String AGENT_SYSTEM_NAME_VERSION = "OperatingSystemNameVersion";

    public static final String AGENT_SYSTEM_NAME = "OperatingSystemName";

    public static final String AGENT_NAME_VERSION = "AgentNameVersion";

    public static final String AGENT_OS_VERSION_BUILD = "OperatingSystemVersionBuild";

    public static final String DEVICE_ANDROID = "android";

    public static final String DEVICE_IOS = "ios";

    public static final String DEVICE_MAC_OS = "mac os";

}
