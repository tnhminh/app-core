package com.adsplay.conf;

import java.util.HashMap;

/**
 * User: khoivu
 * Date: 8/26/18
 * Time: 11:25 PM
 */
public class TopicsConfig {

    public transient static String SINK_PAYTV_TRACKING = "sink_paytv_tracking";
    public transient static String SINK_FPLAY_TRACKING = "sink_fplay_tracking";
    public transient static String SINK_WIFI_TRACKING = "sink_wifi_tracking";
    public transient static String SINK_PAYTV_DELIVERY = "sink_paytv_delivery";
    public transient static String SINK_FPLAY_DELIVERY = "sink_fplay_delivery";
    public transient static String SINK_WIFI_DELIVERY = "sink_wifi_delivery";

    public transient static String SOURCE_PAYTV_TRACKING = "paytv_tracking";
    public transient static String SOURCE_FPLAY_TRACKING = "fplay_tracking";
    public transient static String SOURCE_WIFI_TRACKING = "wifi_tracking";
    public transient static String SOURCE_PAYTV_DELIVERY = "paytv_delivery";
    public transient static String SOURCE_FPLAY_DELIVERY = "fplay_delivery";
    public transient static String SOURCE_WIFI_DELIVERY = "wifi_delivery";
    public transient static String SOURCE_OTHER_TRACKING = "other_tracking";

    public transient static String SOURCE_OTHER_DELIVERY = "other_delivery";

    private int id;
    private String name;
    private String processorName;
    private String sourceName;
    private String appId;
    private String groupId;
    private boolean active;
    private String executeClass;
    private SinkConfig[] sink;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public String getAppId() {
        return appId;
    }

    public String getProcessorName() {
        return processorName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getExecuteClass() {
        return executeClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProcessorName(String processorName) {
        this.processorName = processorName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setExecuteClass(String executeClass) {
        this.executeClass = executeClass;
    }

    public SinkConfig[] getSink() {
        return sink;
    }

    public void setSink(SinkConfig[] sink) {
        this.sink = sink;
    }
}
