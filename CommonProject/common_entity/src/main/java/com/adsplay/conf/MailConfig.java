package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 10/4/18
 * Time: 11:31 AM
 */
public class MailConfig {

    private String host;
    private int port;
    private String authenticateUser;
    private String authenticatePass;
    private String sender;
    private String pass;
    private String from;
    private boolean startTLSEnabled;
    private boolean enableSSL;

    public boolean isEnableSSL() {
        return enableSSL;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getAuthenticateUser() {
        return authenticateUser;
    }

    public String getAuthenticatePass() {
        return authenticatePass;
    }

    public String getSender() {
        return sender;
    }

    public String getPass() {
        return pass;
    }

    public String getFrom() {
        return from;
    }

    public boolean isStartTLSEnabled() {
        return startTLSEnabled;
    }
}
