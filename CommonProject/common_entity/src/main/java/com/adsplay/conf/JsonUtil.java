package com.adsplay.conf;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * @author khoivu
 */
public class JsonUtil implements Serializable {

    private static final long serialVersionUID = -3301695478208950415L;

    public static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.enable(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
    }

    public static <T> T fromString(Class<T> type, String json) throws IOException {
        return mapper.readValue(json, type);
    }

    public static String toString(Object object) throws IOException {
        return mapper.writeValueAsString(object);
    }

    public static String toJsonArray(Object object) throws IOException {
        return mapper.writeValueAsString(object);
    }

    public static boolean isValidJSON(final String json){
        try {
            mapper.readTree(json);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String[] parserToJsonArray(String json) throws IOException {
        return mapper.readValue(json, String[].class);
    }
    
    public static <T> T fromMap(Class<T> type, Map<String, String> map) throws IOException {
        return mapper.convertValue(map, type);
    }
}
