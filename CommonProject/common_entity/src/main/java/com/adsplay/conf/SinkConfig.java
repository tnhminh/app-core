package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 4/26/18
 * Time: 1:46 PM
 */
public class SinkConfig {

    private String name;
    private String topic;

    public String getName() {
        return name;
    }

    public String getTopic() {
        return topic;
    }
}
