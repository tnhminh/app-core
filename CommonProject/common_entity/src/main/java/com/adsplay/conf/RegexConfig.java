package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 5/7/18
 * Time: 5:02 PM
 */
public class RegexConfig {

    private String splitParams;
    private String versionIos;
    private String deviceIos;
    private String versionAndroid;
    private String deviceAndroid;

    public String getSplitParams() {
        return splitParams;
    }

    public String getVersionIos() {
        return versionIos;
    }

    public String getDeviceIos() {
        return deviceIos;
    }

    public String getVersionAndroid() {
        return versionAndroid;
    }

    public String getDeviceAndroid() {
        return deviceAndroid;
    }
}
