package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 9/19/18
 * Time: 5:19 PM
 */
public class DateTimeConfig {

    private String date;
    private String hour;
    private String dateTime;
    private String receiveTime;

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getReceiveTime() {
        return receiveTime;
    }
}
