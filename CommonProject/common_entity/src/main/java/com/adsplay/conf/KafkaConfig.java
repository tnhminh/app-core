package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 4/26/18
 * Time: 1:45 PM
 */
public class KafkaConfig {

    private String server;

    private boolean active = false;

    private int numBroker;

    public boolean isActive() {
        return active;
    }

    public String getServer() {
        return server;
    }

    public int getNumBroker() {
        return numBroker;
    }

}
