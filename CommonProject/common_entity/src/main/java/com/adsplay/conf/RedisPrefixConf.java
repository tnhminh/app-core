package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 10/17/18
 * Time: 10:35 AM
 */
public class RedisPrefixConf {

    private String prefixIP;
    private String prefixPlacement;
    private String prefixWifiLocation;
    private String prefixCategory;
    private String prefixContent;
    private String prefixProvince;

    public String getPrefixProvince() {
        return prefixProvince;
    }

    public String getPrefixCategory() {
        return prefixCategory;
    }

    public String getPrefixContent() {
        return prefixContent;
    }

    public String getPrefixIP() {
        return prefixIP;
    }

    public String getPrefixPlacement() {
        return prefixPlacement;
    }

    public String getPrefixWifiLocation() {
        return prefixWifiLocation;
    }
}
