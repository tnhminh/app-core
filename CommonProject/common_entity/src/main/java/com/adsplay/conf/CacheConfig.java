package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 8/29/18
 * Time: 2:38 PM
 */
public class CacheConfig {

    private long longExpired;
    private long mediumExpired;
    private long shortExpired;

    public long getLongExpired() {
        return longExpired;
    }

    public long getMediumExpired() {
        return mediumExpired;
    }

    public long getShortExpired() {
        return shortExpired;
    }
}
