package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 24/01/19
 * Time: 15:10
 */
public class GrayLogConfig {

    private String host;

    private int port;

    private String source;

    private String message;

    private boolean active = false;

    private int queueSize;

    private int timeout;

    private int reconnectDelay;

    private int bufferSize;

    public boolean isActive() {
        return active;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getSource() {
        return source;
    }

    public String getMessage() {
        return message;
    }

    public int getQueueSize() {
        return queueSize;
    }

    public int getTimeout() {
        return timeout;
    }

    public int getReconnectDelay() {
        return reconnectDelay;
    }

    public int getBufferSize() {
        return bufferSize;
    }
}
