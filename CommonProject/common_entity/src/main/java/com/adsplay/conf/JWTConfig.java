package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 10/4/18
 * Time: 10:32 AM
 */
public class JWTConfig {

    private String secretKey = "secretKey";
    private int expirationInMinute = 30;

    public String getSecretKey() {
        return secretKey;
    }

    public int getExpirationInMinute() {
        return expirationInMinute;
    }
}
