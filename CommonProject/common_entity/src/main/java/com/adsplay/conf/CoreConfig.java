package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 8/24/18
 * Time: 3:35 PM
 */
public class CoreConfig {

    private int timeout;

    private JWTConfig jwt;

    private MailConfig mail;

    private RedisPrefixConf redisPrefix;

    private String formatDate;

    private GrayLogConfig grayLog;

    private String sentryDns = "";

    private String sentryEmail = "";

    public GrayLogConfig getGrayLog() {
        return grayLog;
    }

    public String getFormatDate() {
        return formatDate;
    }

    public String getSentryDns() {
        return sentryDns;
    }

    public String getSentryEmail() {
        return sentryEmail;
    }

    public RedisPrefixConf getRedisPrefix() {
        return redisPrefix;
    }

    public MailConfig getMail() {
        return mail;
    }

    public JWTConfig getJwt() {
        return jwt;
    }

    private CacheConfig cache;

    public CacheConfig getCache() {
        return cache;
    }

    public int getTimeout() {
        return timeout;
    }
}
