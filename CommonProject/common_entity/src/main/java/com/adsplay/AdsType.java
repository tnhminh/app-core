package com.adsplay;

/**
 * User: khoivu
 * Date: 9/27/18
 * Time: 2:03 PM
 */
public enum  AdsType {

    PAY_TV                  ("pay-tv", "Pay TV"),
    FPT_PLAY                ("fpt-play", "FPT Play"),
    LIVE_TV                 ("live-tv", "Live TV"),
    WIFI_BANNER             ("wifi-banner", "Wifi Banner"),
    OTHER                   ("", "OTHER")
    ;

    private final String key;
    private final String value;

    AdsType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public static String findValue(String key) {
        for (AdsType adsType : values()) {
            if (adsType.getKey().equals(key)) {
                return adsType.getValue();
            }
        }
        return AdsType.OTHER.getValue();
    }
}
