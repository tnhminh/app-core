package com.adsplay.entity;

/**
 * User: khoivu
 * Date: 10/15/18
 * Time: 10:20 PM
 */
public class Accounts {
    private int id;
    private String name;
    private String model;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
