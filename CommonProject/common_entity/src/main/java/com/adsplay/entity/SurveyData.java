package com.adsplay.entity;

/**
 * User: khoivu
 * Date: 10/10/18
 * Time: 10:50 AM
 */
public class SurveyData {

    private long id;
    private long surveyName;
    private String name;
    private String phoneNumber;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(long surveyName) {
        this.surveyName = surveyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
