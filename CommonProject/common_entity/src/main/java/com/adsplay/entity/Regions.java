package com.adsplay.entity;

/**
 * User: khoivu
 * Date: 9/30/18
 * Time: 8:24 PM
 */
public class Regions {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
