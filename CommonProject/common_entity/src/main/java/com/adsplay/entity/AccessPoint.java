package com.adsplay.entity;

/**
 * User: khoivu
 * Date: 10/9/18
 * Time: 10:18 AM
 */
public class AccessPoint {

    private int id;
    private String name;
    private String macAddress;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
