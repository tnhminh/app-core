package com.adsplay.entity;

/**
 * User: khoivu
 * Date: 9/25/18
 * Time: 3:08 PM
 */
public class Placement {

    private int id;
    private String name;
    private String publisherCode;
    private String adView;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublisherCode() {
        return publisherCode;
    }

    public void setPublisherCode(String publisherCode) {
        this.publisherCode = publisherCode;
    }

    public String getAdView() {
        return adView;
    }

    public void setAdView(String adView) {
        this.adView = adView;
    }
}
