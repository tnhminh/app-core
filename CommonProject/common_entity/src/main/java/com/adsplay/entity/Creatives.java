package com.adsplay.entity;

/**
 * User: khoivu
 * Date: 8/29/18
 * Time: 3:08 PM
 */
public class Creatives {

    private int id;
    private String name;
    private int flightId;
    private String adView;
    private String landingPage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getAdView() {
        return adView;
    }

    public void setAdView(String adView) {
        this.adView = adView;
    }

    public String getLandingPage() {
        return landingPage;
    }

    public void setLandingPage(String landingPage) {
        this.landingPage = landingPage;
    }
}
