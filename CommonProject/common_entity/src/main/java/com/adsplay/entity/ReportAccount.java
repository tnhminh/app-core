package com.adsplay.entity;

import java.sql.Timestamp;

/**
 * User: khoivu
 * Date: 10/4/18
 * Time: 10:07 AM
 */
public class ReportAccount {

    private String user;
    private String password;
    private int role;
    private Timestamp lastPasswordReset;

    public Timestamp getLastPasswordReset() {
        return lastPasswordReset;
    }

    public void setLastPasswordReset(Timestamp lastPasswordReset) {
        this.lastPasswordReset = lastPasswordReset;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
}
