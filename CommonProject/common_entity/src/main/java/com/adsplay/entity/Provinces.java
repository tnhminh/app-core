package com.adsplay.entity;

/**
 * User: khoivu
 * Date: 9/30/18
 * Time: 8:24 PM
 */
public class Provinces {

    private int id;
    private String name;
    private String publisherCode;

    public String getPublisherCode() {
        return publisherCode;
    }

    public void setPublisherCode(String publisherCode) {
        this.publisherCode = publisherCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
