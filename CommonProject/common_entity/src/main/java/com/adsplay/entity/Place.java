package com.adsplay.entity;

/**
 * User: khoivu
 * Date: 9/25/18
 * Time: 3:08 PM
 */
public class Place {

    private int id;
    private String name;
    private String refCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }
}
