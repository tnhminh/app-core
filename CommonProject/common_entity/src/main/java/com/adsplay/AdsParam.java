package com.adsplay;

/**
 * User: khoivu, updated by Minh Tran
 * Date: 4/13/18
 * Time: 10:55 AM
 */
public enum AdsParam {

    //Key dbname redis cache
    REDIS_RENDER_CLUSTER                        ("clusterInfoRedis"),
    REDIS_RENDER_API_DATA                       ("apiData"),
    REDIS_RENDER_FLIGHT_DATA                    ("flightData"),
    REDIS_RENDER_FLIGHT_WEIGHT_DATA             ("flightWeightData"),
    REDIS_RENDER_PLACEMENT_DATA                 ("placementData"),
    REDIS_RENDER_BOOKING_DATA                   ("bookingData"),
    REDIS_RENDER_CREATIVE_DATA                  ("creativeData"),
    REDIS_CACHE_DELIVERY                        ("deliveryCache"),
    REDIS_REALTIME_BOOKING                      ("realtimeBooking"),
    REDIS_KEY_PREFIX_BOOKING                    ("bookings_"),

    //Params
    PARAM_MOVIE_ID                              ("movieId"),
    PARAM_IS_PAYTV                              ("isPayTV"),
    PARAM_CON_ID                                ("conID"),
    PARAM_COPYRIGHT_ID                          ("copyrightId"),
    PARAM_CATEGORY                              ("catID"),
    PARAM_PARENT_CATEGORY                       ("parentCat"),
    PARAM_PAGE_URL                              ("url"),
    PARAM_LOCATION                              ("loc"),
    PARAM_CONTENT_ID                            ("ctid"),
    PARAM_CID                                   ("cid"),

    //REDIS
    DATE_RANGE                                  ("date_range"),
    DAY_WEEKS                                   ("day_weeks"),
    HOURS                                       ("hours"),
    TIMEZONE                                    ("timezone"),
    SOURCE_PROVIDER                             ("source_providers"),
    CATEGORIES                                  ("categories"),
    LOCATION                                    ("location"),

    //Key add redis
    REDIS_UTILS_EMPTY_VAL                       ("0"),
    REDIS_UTILS_TOTAL                           ("total"),
    REDIS_UTILS_DAILY                           ("daily"),
    REDIS_UTILS_BOOKING                         ("booking"),
    REDIS_UTILS_UNDERLINE                       ("_"),
    REDIS_UTILS_BK                              ("bk"),

    //Key load redis cache init
    RETARGETING_SOURCE                                ("Retargeting_Source"),
    DELIVERY_CACHE_REDIS                                ("DeliveryCache"),
    NORMAL_CASE_SOURCE                          ("Normal_Source"),
    EVENT_SOURCE                           ("Event_Source"),
    DEBUGGER_CLIENT_SOURCE                           ("Debugger_Source"),
    UNKOWN                                      ("unknown");

    public String name;

    AdsParam(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static AdsParam getTypeByName(String name) {
        for (AdsParam type : values()) {
            if (type.getName().equals(name)) {
                return type;
            }
        }
        return UNKOWN;
    }
    
    public static void main(String[] args) {
        getTypeByName("");
    }

}
