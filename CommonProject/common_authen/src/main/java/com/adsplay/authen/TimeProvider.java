package com.adsplay.authen;

import java.io.Serializable;
import java.util.Date;

/**
 * User: khoivu
 * Date: 10/3/18
 * Time: 3:18 PM
 */
public class TimeProvider implements Serializable {

    private static final long serialVersionUID = -3301695478208950415L;

    public static Date now() {
        return new Date();
    }
}
