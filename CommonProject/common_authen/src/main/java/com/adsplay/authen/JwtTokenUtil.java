package com.adsplay.authen;

import com.adsplay.Constant;
import com.adsplay.entity.ReportAccount;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -3301605591108950415L;
    private static final String AUDIENCE_ADMIN = "adsplay";

    private static String SECRET_KEY;
    private static long EXPIRED;

    public JwtTokenUtil(String secret, long expired) {
        SECRET_KEY = secret;
        EXPIRED = expired;
    }

    public static String getUsernameFromToken(String token) {
        return getClaimFromToken(token).getSubject();
    }

    public static Date getIssuedAtDateFromToken(String token) {
        return getClaimFromToken(token).getIssuedAt();
    }

    public static Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token).getExpiration();
    }

    public String getAudienceFromToken(String token) {
        return getClaimFromToken(token).getAudience();
    }

    public static String getRoleFromToken(String token) {
        return (String) getClaimFromToken(token).get(Constant.ROLE);
    }

    public static Claims getClaimFromToken(String token) {
        return getAllClaimsFromToken(token);
    }

    private static Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    private static Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(TimeProvider.now());
    }

    private static Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
        return (lastPasswordReset != null && created.before(lastPasswordReset));
    }

    private Boolean ignoreTokenExpiration(String token) {
        String audience = getAudienceFromToken(token);
        return AUDIENCE_ADMIN.equals(audience);
    }

    public static String generateToken(String user, String pass, String role) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(Constant.USER, user);
        claims.put(Constant.PASS, pass);
        claims.put(Constant.ROLE, role);
        return doGenerateToken(claims, user, AUDIENCE_ADMIN);
    }

    private static String doGenerateToken(Map<String, Object> claims, String subject, String audience) {
        final Date createdDate = TimeProvider.now();
        final Date expirationDate = calculateExpirationDate(createdDate);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setAudience(audience)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    public Boolean canTokenBeRefreshed(String token, Date lastPasswordReset) {
        final Date created = getIssuedAtDateFromToken(token);
        return !isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
                && (!isTokenExpired(token) || ignoreTokenExpiration(token));
    }

    public String refreshToken(String token) {
        final Date createdDate = TimeProvider.now();
        final Date expirationDate = calculateExpirationDate(createdDate);

        final Claims claims = getAllClaimsFromToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    public static Boolean validateToken(String token, ReportAccount user) {
        final String username = getUsernameFromToken(token);
        final Date created = getIssuedAtDateFromToken(token);
        final String role = getRoleFromToken(token);
        //final Date expiration = getExpirationDateFromToken(token);
        return (
                username.equals(user.getUser()) && !isTokenExpired(token) && !isCreatedBeforeLastPasswordReset(created, user.getLastPasswordReset())
        );
    }

    private static Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + EXPIRED * 1000L * 60L);
    }
}
