package com.adsplay.authen;

import com.nexmo.client.NexmoClient;
import com.nexmo.client.NexmoClientException;
import com.nexmo.client.auth.AuthMethod;
import com.nexmo.client.auth.TokenAuthMethod;
import com.nexmo.client.sms.SmsClient;
import com.nexmo.client.sms.SmsSubmissionResult;
import com.nexmo.client.sms.messages.TextMessage;

import java.io.IOException;

/**
 * User: khoivu
 * Date: 10/4/18
 * Time: 4:05 PM
 */
public class SendSMS {

    public static void main(String[] args) throws IOException, NexmoClientException {
        
        AuthMethod auth = new TokenAuthMethod("969f452f", "v2E86vtA2VtCtmRw");
        SmsClient client = new NexmoClient(auth).getSmsClient();
        TextMessage message = new TextMessage(
                "Adsplay",
                "84902609819",
                "------------------123456------------------"
        );
        SmsSubmissionResult[] responses = client.submitMessage(message);
        for (SmsSubmissionResult response : responses) {
            System.out.println("-------------------------");
            System.out.println(response);
        }


    }
}
