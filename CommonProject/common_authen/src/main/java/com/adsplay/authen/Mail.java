package com.adsplay.authen;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.conf.MailConfig;
import com.nexmo.client.NexmoClientException;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import javax.mail.*;
import java.io.IOException;

/**
 * User: khoivu
 * Date: 10/4/18
 * Time: 11:12 AM
 */
public class Mail {

    private static Mail mail;

    private final MailConfig mailConfig;

    private Mail(MailConfig mailConfig) {
        this.mailConfig = mailConfig;
    }

    public static void main(String[] args) throws IOException, EmailException, NexmoClientException {
       /* MultiLog.setLogParentFolder("log");
        String jsonCoreConfig = FileUtils.readFileToString(new File("/Users/khoivu/Google Drive/Project/FTEL/report-service/gateway_survey/conf/core_config.json"), StandardCharsets.UTF_8);
        CoreConfig coreConfig = JsonUtil.fromString(CoreConfig.class, jsonCoreConfig);
        init(coreConfig.getMail());
        getInstance().send(
                "",
                "",
                "khoivg@fpt.com.vn",
                "Test Send Email",
                ""
        );*/

        /*AuthMethod auth = new TokenAuthMethod("969f452f", "v2E86vtA2VtCtmRw");
        SmsClient client = new NexmoClient(auth).getSmsClient();
        TextMessage message = new TextMessage(
                "Adsplay",
                "84902609819",
                "------------------123456------------------"
        );
        SmsSubmissionResult[] responses = client.submitMessage(message);
        for (SmsSubmissionResult response : responses) {
            System.out.println("-------------------------");
            System.out.println(response);
        }*/

        HtmlEmail email = new HtmlEmail();
        email.setHostName("mail02.fptplay.net");
        email.setSmtpPort(25);
        email.setAuthentication("info@adsplay.net", "3s4zQ92FcH");
        email.setSSLOnConnect(true);
        email.setStartTLSEnabled(true);
        email.setStartTLSRequired(true);
        email.setFrom("info@adsplay.net", "Khoi Vu");

        email.addTo("info@adsplay.net");
        email.setSubject("Tao Thich");
        //email.setHtmlMsg(htmlContent);
        email.setContent("Thich thi sao", "text/html; charset=UTF-8");
        email.send();
    }

    public static void init(MailConfig mainConfig) {
        mail = new Mail(mainConfig);
    }

    public static Mail getInstance() {
        if (mail == null) {
            throw new NullPointerException("Please invoke the Init method FIRST. ONLY ONE TIME!");
        }
        return mail;
    }

    public void send(String processName, String requestId, String to, String subject, String htmlContent) {
        try {
            HtmlEmail email = new HtmlEmail();
            email.setHostName(mailConfig.getHost());
            email.setAuthentication(mailConfig.getAuthenticateUser(), mailConfig.getAuthenticatePass());
            email.setSSLOnConnect(mailConfig.isEnableSSL());
            email.setSmtpPort(mailConfig.getPort());
            email.setStartTLSEnabled(mailConfig.isStartTLSEnabled());
            email.setStartTLSRequired(mailConfig.isStartTLSEnabled());
            email.addBcc(mailConfig.getFrom());
            email.setFrom(mailConfig.getFrom(), mailConfig.getSender());

            email.addTo(to);
            email.setSubject(subject);
            //email.setHtmlMsg(htmlContent);
            email.setContent(htmlContent, "text/html; charset=UTF-8");
            email.send();
//            copyIntoSent(email.getMailSession(), email.getMimeMessage());
            MultiLog.write(processName, requestId, "Send Email to [" + to + "] Success!!");
        } catch (Exception ex) {
            MultiLog.error(processName, requestId, "Send Email to [" + to + "] Fail!!", ex);
        }
    }

    /**
     * function copyIntoSent use for copy message into Sent which mask one mail
     * been sent.
     */
    private void copyIntoSent(final Session session, final Message msg) throws MessagingException {
        final Store store = session.getStore("imaps");
        store.connect(mailConfig.getHost(), mailConfig.getFrom(), mailConfig.getPass());
        final Folder folder = (Folder) store.getFolder("Sent");
        if (folder.exists() == false) {
            folder.create(Folder.HOLDS_MESSAGES);
        }
        folder.open(Folder.READ_WRITE);
        folder.appendMessages(new Message[]{msg});
    }

}
