package com.adsplay.conf;

import com.adsplay.AdsParam;

/**
 * User: khoivu
 * Date: 4/11/18
 * Time: 11:12 PM
 */
public class RedisConfig {

    private String name;
    private String host;
    private int port;
    private int poolSize;

    public AdsParam getAdsConst(String name) {
        return AdsParam.getTypeByName(name);
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public int getPoolSize() {
        return poolSize;
    }

}
