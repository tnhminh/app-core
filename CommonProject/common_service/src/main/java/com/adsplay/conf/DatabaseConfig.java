package com.adsplay.conf;

import java.util.List;

/**
 * User: khoivu
 * Date: 8/24/18
 * Time: 5:45 PM
 */
public class DatabaseConfig {

    private static final long serialVersionUID = -8463846509803411615L;

    public transient static final String DATABASE_POSTGRES = "postgres";

    public transient static final String DATABASE_CLICKHOUSE = "clickhouse";

    public transient static final String SCHEMA_ADSPLAY = "adsplay";

    public transient static final String SCHEMA_ADSPLAY_REPORT = "adsplay_report";

    public transient static final String SCHEMA_DEV_ADSPLAY_REPORT = "adsplay_dev";

    private String username;

    private String password;

    private String host;

    private int port;

    private int timeout;

    private List<String> dbName;

    private boolean active = false;

    private String connectionName;

    private int maxConnection;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public boolean isActive() {
        return active;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public int getTimeout() {
        return timeout;
    }

    public List<String> getDbName() {
        return dbName;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public int getMaxConnection() {
        return maxConnection;
    }
}
