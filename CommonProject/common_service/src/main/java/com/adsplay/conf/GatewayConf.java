package com.adsplay.conf;

/**
 * User: khoivu
 * Date: 8/26/18
 * Time: 5:07 PM
 */
public class GatewayConf {

    private String logDir;

    private String initClass;

    private String initConfPath;

    private String moduleConfPath;

    private int sizeInsert = 5000;

    private int instances = 3;

    private boolean disableVerify;

    private boolean verifyHeader;

    private int timeout;

    private long limitTime;

    private int port;

    private int adminPort;

    private int poolSize;

    private String projectName;

    private String queryPath;
    
    private String logDomain;
    
    private String protocol;

    public int getInstances() {
        return instances;
    }

    public String getQueryPath() {
        return queryPath;
    }

    public String getModuleConfPath() {
        return moduleConfPath;
    }

    public int getPoolSize() {
        return poolSize;
    }

    public String getInitConfPath() {
        return initConfPath;
    }

    public int getSizeInsert() {
        return sizeInsert;
    }

    public long getLimitTime() {
        return limitTime;
    }

    public boolean isVerifyHeader() {
        return verifyHeader;
    }

    public boolean isDisableVerify() {
        return disableVerify;
    }

    public String getInitClass() {
        return initClass;
    }

    public String getLogDir() {
        return logDir;
    }

    public int getTimeout() {
        return timeout;
    }

    public int getPort() {
        return port;
    }

    public int getAdminPort() {
        return adminPort;
    }

    public String getProjectName() {
        return projectName;
    }

    public String getLogDomain() {
        return logDomain;
    }

    public void setLogDomain(String logDomain) {
        this.logDomain = logDomain;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
}
