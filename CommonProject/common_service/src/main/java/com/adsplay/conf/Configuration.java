package com.adsplay.conf;

/**
 * @author khoivu
 */
public class Configuration {

    private static Configuration configuration;

    private final GatewayConf gatewayConf;

    private final DataManagerConfig dataManagerConfig;

    private Configuration(String jsonCoreConfig, String jsonGatewayConfig, String jsonDataManagerConfig) throws Exception {
        this.gatewayConf = JsonUtil.fromString(GatewayConf.class, jsonGatewayConfig);
        this.dataManagerConfig = JsonUtil.fromString(DataManagerConfig.class, jsonDataManagerConfig);

    }

    public static Configuration getInstance() {
        return configuration;
    }

    public static void init(String jsonCoreConfig, String jsonGatewayConfig, String jsonDataManagerConfig) throws Exception {
        configuration = new Configuration(jsonCoreConfig, jsonGatewayConfig, jsonDataManagerConfig);
    }

    public GatewayConf getGatewayConf() {
        return gatewayConf;
    }

    public DataManagerConfig getDataManagerConfig() {
        return dataManagerConfig;
    }

}
