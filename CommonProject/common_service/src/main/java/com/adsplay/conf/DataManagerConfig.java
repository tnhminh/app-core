package com.adsplay.conf;

import java.util.List;

/**
 * User: khoivu
 * Date: 5/15/18
 * Time: 1:50 PM
 */
public class DataManagerConfig {

    private String queryPath;

    private KafkaConfig kafka;

    private boolean test = false;

    private DatabaseConfig clickhouse;

    private DatabaseConfig postgres;

    private List<RedisConfig> redisConfig;

    public List<RedisConfig> getRedisConfig() {
        return redisConfig;
    }

    public boolean isTest() {
        return test;
    }

    public String getQueryPath() {
        return queryPath;
    }

    public KafkaConfig getKafka() {
        return kafka;
    }

    public DatabaseConfig getClickhouse() {
        return clickhouse;
    }

    public DatabaseConfig getPostgres() {
        return postgres;
    }

}
