package com.adsplay.database;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.conf.JsonUtil;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.lambdaworks.redis.support.ConnectionPoolSupport;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * User: khoivu
 * Date: 4/13/18
 * Time: 9:56 AM
 */
public class Redis {

    private final GenericObjectPool<StatefulRedisConnection<String, String>> pool;
    private final ExecutorService executorService;

    public static void main(String[] args) throws IOException {
       /* Redis redis = new Redis("42.116.8.188", 6380, 10);
        List<String> result = redis.getAll(
                "Test",
                System.currentTimeMillis() + "",
                "bk_68_hourly_2018071115_impression_*"
        );
        System.out.println(JsonUtil.toString(result));*/

        Redis redis = new Redis("42.116.8.188", 6382, 10);
        Map<String, String> result = redis.getCacheAll(
                "Test",
                System.currentTimeMillis() + "",
                "inv_306_datehour_2018121708_request"
        );
        System.out.println(JsonUtil.toString(result.get("video")));
    }

    /**
     * Create new redis client by host/port
     * @param host
     * @param port
     * @param poolsize
     */
    protected Redis(String host, int port, int poolsize) {
        RedisURI redisURI = RedisURI.create(host, port);
        RedisClient redisClient = RedisClient.create(redisURI);
        redisClient.setDefaultTimeout(1, TimeUnit.SECONDS);

        GenericObjectPoolConfig conf = new GenericObjectPoolConfig();
        conf.setMinIdle(10);
        conf.setMaxIdle(32);
        conf.setMaxTotal(40);

        pool = ConnectionPoolSupport.createGenericObjectPool(redisClient::connect, conf);
        executorService = Executors.newFixedThreadPool(poolsize);

    }

    /**
     * 'hset' command
     * @param processName
     * @param requestId
     * @param dbKey
     * @param key
     * @param data
     */
    public void addCache(String processName, Object requestId, String dbKey, String key, String data) {
        executorService.submit(() -> {
            try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
                RedisCommands<String, String> sync = connection.sync();
                boolean result = sync.hset(dbKey, key, data);
                MultiLog.write(processName, requestId, "Store Data to cache: [" + result + "]");
            } catch (Exception ex) {
                MultiLog.error(processName, requestId, key + " - " + data, "Error when Redis addCache data to cache", ex);

            }
        });
    }

    /**
     * 'get' command by key
     * @param processName
     * @param requestId
     * @param key
     * @return String
     */
    public String getCache(String processName, Object requestId, String key) {
        MultiLog.write(processName, requestId, "Request Redis with key: [" + key + "]");
        StatefulRedisConnection<String, String> connection = null;
        try {
            String result;
            connection = pool.borrowObject();
            RedisCommands<String, String> sync = connection.sync();
            result = sync.get(key);
            return result;
        } catch (Exception e) {
            MultiLog.error(processName, requestId, key, "Error when getCache data from Redis", e);
            return null;
        } finally {
            if (connection != null) connection.close();
        }
    }

    /**
     * 'hget' command by using field
     * @param processName
     * @param requestId
     * @param dbKey
     * @param key
     * @return String
     */
    public String getCache(String processName, Object requestId, String dbKey, String key) {
        MultiLog.write(processName, requestId, "Request Redis DBKey: [" + dbKey + "] and key: [" + key + "]");
        StatefulRedisConnection<String, String> connection = null;
        try {
            connection = pool.borrowObject();
            RedisCommands<String, String> sync = connection.sync();
            return sync.hget(dbKey, key);
        } catch (Exception e) {
            MultiLog.error(processName, requestId, dbKey + " - " + key, "Error when getCache data Redis", e);
            return null;
        } finally {
            if (connection != null) connection.close();
        }
    }

    /** 
     * 'hgetall' by using key
     * @param processName
     * @param requestId
     * @param dbKey
     * @return Map<String, String>
     */
    public Map<String, String> getCacheAll(String processName, Object requestId, String dbKey) {
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            RedisCommands<String, String> sync = connection.sync();
            return sync.hgetall(dbKey);

        } catch (Exception e) {
            MultiLog.error(processName, requestId, dbKey, "Error when getCacheAll data from Redis", e);
            return null;
        }
    }

    /**
     * 'Keys' cammand by pattern
     * @param processName
     * @param requestId
     * @param pattern
     * @return List<String>
     */
    public List<String> getAll(String processName, Object requestId, String pattern) {
        MultiLog.write(processName, requestId, "Request Redis get all with pattern: [" + pattern + "]");
        try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
            List<String> keys = connection.sync().keys(pattern);
            Queue<String> value = new LinkedBlockingDeque<>();
            keys.forEach(key -> value.add(connection.sync().get(key)));
            return new ArrayList<>(value);
        } catch (Exception e) {
            MultiLog.error(processName, requestId, pattern, "Error when getAll data from Redis", e);
            return new ArrayList<>();
        }
    }

    /**
     * 'hdel' command by using key
     * @param processName
     * @param requestId
     * @param dbKey
     * @param key
     */
    public void removeCache(String processName, Object requestId, String dbKey, String key) {
        executorService.submit(() -> {
            try (StatefulRedisConnection<String, String> connection = pool.borrowObject()) {
                RedisCommands<String, String> sync = connection.sync();
                long result = sync.hdel(dbKey, key);
                MultiLog.write(processName, requestId, "Delete redis cache result: " + result);
            } catch (Exception e) {
                MultiLog.error(processName, requestId, dbKey + " - " + key, "Error when removeCache cache result from Redis", e);
            }

        });
    }
    
    // Need to implement async process as redis client side
    // TODO
    /**
     * Asyn
     */
    

}
