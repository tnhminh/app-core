package com.adsplay.database;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.*;
import com.adsplay.entity.*;
import com.adsplay.model.CampaignModel;
import com.adsplay.model.CategoryModel;
import com.adsplay.model.ContentModel;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * User: khoivu
 * Date: 1/10/19
 * Time: 10:37 AM
 */
public class DataRepository {

    public static List<GwApi> getRestApis(String processName, String requestId, String projectName) {
        try {
            return DatabaseUtils.executeQuery(
                    DatabaseConfig.DATABASE_POSTGRES,
                    Configuration.getInstance().getDataManagerConfig().isTest() ? DatabaseConfig.SCHEMA_DEV_ADSPLAY_REPORT : DatabaseConfig.SCHEMA_ADSPLAY,
                    processName,
                    requestId,
                    "SELECT * FROM gw_report_api where project_name = ? and active = true",
                    new Object[]{projectName},
                    rs -> {
                        List<GwApi> gwReportApis = new ArrayList<>();
                        try {
                            while (rs.next()) {
                                GwApi reportApi = new GwApi();
                                reportApi.setId(rs.getInt("id"));
                                reportApi.setName(Utils.nullToEmpty(rs.getString("name")));
                                reportApi.setProjectName(Utils.nullToEmpty(rs.getString("project_name")));
                                reportApi.setMethod(Utils.nullToEmpty(rs.getString("method")));
                                reportApi.setContentType(Utils.nullToEmpty(rs.getString("content_type")));
                                reportApi.setAccepted(Utils.nullToEmpty(rs.getString("accepted")));
                                reportApi.setPath(Utils.nullToEmpty(rs.getString("path")));
                                reportApi.setExecuteClass(Utils.nullToEmpty(rs.getString("execute_class")));
                                reportApi.setHeader(rs.getBoolean("is_header"));
                                reportApi.setDisableEncrypt(rs.getBoolean("disable_encrypt"));
                                reportApi.setPoolSize(rs.getInt("poolSize"));
                                reportApi.setType(Utils.nullToEmpty(rs.getString("type")));
                                gwReportApis.add(reportApi);
                            }
                        } catch (Exception e) {
                            MultiLog.error(processName, requestId, e);
                        }
                        return gwReportApis;
                    });
        } catch (Exception e) {
            MultiLog.error(processName, requestId, e);
            return new ArrayList<>();
        }
    }

    public static List<TopicsConfig> getKafkaTopic(String processName, String requestId) {
        try {
            return DatabaseUtils.executeQuery(
                    DatabaseConfig.DATABASE_POSTGRES,
                    Configuration.getInstance().getDataManagerConfig().isTest() ? DatabaseConfig.SCHEMA_DEV_ADSPLAY_REPORT : DatabaseConfig.SCHEMA_ADSPLAY,
                    processName,
                    requestId,
                    "SELECT * FROM kafka_topic_service where project_name = ? and active = true",
                    new Object[]{processName},
                    rs -> {
                        List<TopicsConfig> topics = new ArrayList<>();
                        try {
                            while (rs.next()) {
                                TopicsConfig topic = new TopicsConfig();
                                topic.setId(rs.getInt("id"));
                                topic.setName(rs.getString("name"));
                                topic.setProcessorName(rs.getString("processor_name"));
                                topic.setSourceName(rs.getString("source_name"));
                                topic.setGroupId(rs.getString("group_id"));
                                topic.setAppId(rs.getString("app_id"));
                                topic.setActive(rs.getBoolean("active"));
                                topic.setExecuteClass(Utils.nullToEmpty(rs.getString("execute_class")));
                                String sinkValue = Utils.nullToEmpty(rs.getString("sink"));
                                if (!Utils.isEmpty(sinkValue)) {
                                    topic.setSink(JsonUtil.fromString(SinkConfig[].class, sinkValue));
                                }
                                topics.add(topic);
                            }
                        } catch (Exception e) {
                            MultiLog.error(processName, requestId, e);
                        }
                        return topics;

                    }
            );
        } catch (Exception e) {
            MultiLog.error(processName, requestId, e);
            return new ArrayList<>();
        }
    }
}
