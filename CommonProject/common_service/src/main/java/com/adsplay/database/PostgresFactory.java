package com.adsplay.database;

import com.adsplay.common.logs.WriteLog;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.Configuration;
import com.adsplay.conf.DatabaseConfig;
import org.postgresql.ds.PGConnectionPoolDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * User: khoivu
 * Date: 8/26/18
 * Time: 2:33 PM
 */
public class PostgresFactory extends ConnectionFactory {

    private static PostgresFactory instance = new PostgresFactory();

    private static PGConnectionPoolDataSource dataSource;

    private static PGConnectionPoolDataSource dataSourceReport;

    private static PGConnectionPoolDataSource dataDevSourceReport;

    private DatabaseConfig config;

    @Override
    public void init(String processName, DatabaseConfig config) {
        setConfig(config);
        getConfig().getDbName().forEach(dbName -> {
            if (dbName.equals(DatabaseConfig.SCHEMA_ADSPLAY)) {
                dataSource = createPool(processName, config, dbName);
            } else if (dbName.equals(DatabaseConfig.SCHEMA_ADSPLAY_REPORT)) {
                dataSourceReport = createPool(processName, config, dbName);
            } else if (dbName.equals(DatabaseConfig.SCHEMA_DEV_ADSPLAY_REPORT)) {
                dataDevSourceReport = createPool(processName, config, dbName);
            }
        });
    }

    public static PostgresFactory getInstance() {
        return instance;
    }

    private static PGConnectionPoolDataSource createPool(String processName, DatabaseConfig config, String dbName) {
        WriteLog.write(processName, "DATABASE", "---------------------------------------");
        WriteLog.write(processName, "DATABASE", "url [" + config.getHost() + ":" + config.getPort() + "],user [" + config.getUsername() + "],dbName [" + dbName + "]");
        WriteLog.write(processName, "DATABASE", "----------------------------------------");

        PGConnectionPoolDataSource source = new PGConnectionPoolDataSource();
        source.setServerName(config.getHost());
        source.setPortNumber(config.getPort());
        source.setDatabaseName(dbName);
        source.setUser(config.getUsername());
        source.setPassword(config.getPassword());
        source.setConnectTimeout(config.getTimeout());
        WriteLog.write(processName, "DATABASE", "CREATE POOL:...DONE!!");
        return source;
    }

    private static PGConnectionPoolDataSource getPoolDataSource(String dbName) throws SQLException {
        if (Configuration.getInstance().getDataManagerConfig().isTest()) {
            dbName = DatabaseConfig.SCHEMA_DEV_ADSPLAY_REPORT;
        }
        if (Utils.isEmpty(dbName) || dbName.equals(DatabaseConfig.SCHEMA_ADSPLAY)) {
            return dataSource;
        } else if (dbName.equals(DatabaseConfig.SCHEMA_ADSPLAY_REPORT)) {
            return dataSourceReport;
        } else if (dbName.equals(DatabaseConfig.SCHEMA_DEV_ADSPLAY_REPORT)) {
            return dataDevSourceReport;
        } else {
            throw new SQLException();
        }
    }

    public DatabaseConfig getConfig() {
        return config;
    }

    public void setConfig(DatabaseConfig config) {
        this.config = config;
    }

    @Override
    public Connection getConnection(String serviceCode, String requestId) throws SQLException {
        Connection connection;
        do {
            connection = getPoolDataSource("").getConnection();
        }
        while (connection == null);
        connection.setAutoCommit(true);
        return connection;
    }

    @Override
    public Connection getConnection(String serviceCode, String requestId, String dbName) throws SQLException {
        Connection connection;
        do {
            connection = getPoolDataSource(dbName).getConnection();
        }
        while (connection == null);
        connection.setAutoCommit(true);
        return connection;
    }
}
