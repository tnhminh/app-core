package com.adsplay.database;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.DatabaseConfig;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * User: trungnguyen
 * Date: 12/22/18
 * Time: 10:04 AM
 */
public class DatabaseUtils {

    private static void writeSqlParams(String processName, String requestId, Object[] params) {
        if (null != params) {
            for (int index = 0; index < params.length; index++) {
                MultiLog.write(processName, requestId, "param :" + index + 1 + " value:" + params[index]);
            }
        }
    }

    public static <R> R executeQuery(String databaseType,
                                     String dbName,
                                     String processName,
                                     String requestId,
                                     String sql,
                                     Object[] params, Function<ResultSet, R> callback) throws Exception {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            MultiLog.write(processName, requestId, "Query: " + sql);
            writeSqlParams(processName, requestId, params);
            if (databaseType.equals(DatabaseConfig.DATABASE_POSTGRES)) {
                if (Utils.isEmpty(dbName) || dbName.equals(DatabaseConfig.SCHEMA_ADSPLAY)) {
                    conn = PostgresFactory.getInstance().getConnection();
                } else {
                    conn = PostgresFactory.getInstance().getConnection(dbName);
                }
            }
            preparedStatement = conn.prepareStatement(sql);
            setParams(preparedStatement, params);
            rs = preparedStatement.executeQuery();
            return callback.apply(rs);
        } catch (Exception ex) {
            String message = ex.getMessage();
            if (ConnectionFactory.checkErrorMessage(message)) {
                ConnectionFactory.close(conn);
                MultiLog.write(processName, requestId, "execute query again:" + sql);
                executeQuery(databaseType, dbName, processName, requestId, sql, params, callback);
            }
            MultiLog.error(processName, requestId, ex);
            throw new Exception();
        } finally {
            Utils.close(conn);
            Utils.close(preparedStatement);
            Utils.close(rs);
        }
    }

    public static <T> Result<T> executeQuery(String databaseType,
                                             String dbName,
                                             String processName,
                                             String requestId,
                                             String sql,
                                             Object[] params,
                                             Class<T> resultType) throws Exception {
        ResultSetMapper resultSetMapper = new ResultSetMapper<T>();
        return executeQuery(databaseType, dbName, processName, requestId, sql, params, rs -> {
            Result result = new Result();
            if (null != rs) {
                List list = resultSetMapper.mapRersultSetToObject(processName, requestId, rs, resultType);
                result.list = list;
            }
            return result;
        });
    }

    public static void executeUpdate(String databaseType,
                                     String dbName,
                                     String processName,
                                     String requestId,
                                     String sql,
                                     Object[] params) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            MultiLog.write(processName, requestId, "Query: " + sql);
            writeSqlParams(processName, requestId, params);
            if (databaseType.equals(DatabaseConfig.DATABASE_POSTGRES)) {
                if (Utils.isEmpty(dbName) || dbName.equals(DatabaseConfig.SCHEMA_ADSPLAY)) {
                    conn = PostgresFactory.getInstance().getConnection();
                } else {
                    conn = PostgresFactory.getInstance().getConnection(dbName);
                }
            }
            preparedStatement = conn.prepareStatement(sql);
            setParams(preparedStatement, params);
            preparedStatement.executeUpdate();
        } catch (Exception ex) {
            String message = ex.getMessage();
            if (ConnectionFactory.checkErrorMessage(message)) {
                ConnectionFactory.close(conn);
                MultiLog.write(processName, requestId, "execute query again:" + sql);
                executeUpdate(databaseType, dbName, processName, requestId, sql, params);
            }
            MultiLog.write(processName, requestId, ex);
            //            throw new GwException(DefaultCode.OTHER_ERROR);
        } finally {
            Utils.close(conn);
            Utils.close(preparedStatement);
        }
    }

    public static void executeUpdate(String databaseType,
                                     String dbName,
                                     String processName,
                                     String requestId,
                                     String sql,
                                     List<Object[]> params) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        try {
            MultiLog.write(processName, requestId, "Query: " + sql);
            //            writeSqlParams(processName, requestId, params);
            if (databaseType.equals(DatabaseConfig.DATABASE_POSTGRES)) {
                if (Utils.isEmpty(dbName) || dbName.equals(DatabaseConfig.SCHEMA_ADSPLAY)) {
                    conn = PostgresFactory.getInstance().getConnection();
                } else {
                    conn = PostgresFactory.getInstance().getConnection(dbName);
                }
            }
            preparedStatement = conn.prepareStatement(sql);
            for (Object[] param : params) {
                try {
                    setParams(preparedStatement, param);
                    preparedStatement.addBatch();
                } catch (Exception e) {
                    MultiLog.error(processName, requestId, e);
                }
            }

            preparedStatement.executeBatch();
        } catch (Exception ex) {
            String message = ex.getMessage();
            if (ConnectionFactory.checkErrorMessage(message)) {
                ConnectionFactory.close(conn);
                MultiLog.write(processName, requestId, "execute query again:" + sql);
                executeUpdate(databaseType, dbName, processName, requestId, sql, params);
            }
            MultiLog.error(processName, requestId, ex);
            //            throw new GwException(DefaultCode.OTHER_ERROR);
        } finally {
            Utils.close(conn);
            Utils.close(preparedStatement);
        }
    }

    private static void setParams(PreparedStatement preparedStatement, Object[] params) throws Exception {
        if (null != params) {
            for (int index = 0; index < params.length; index++) {
                Object value = params[index];
                if (value instanceof Integer) {
                    preparedStatement.setInt(index + 1, (Integer) value);
                } else if (value instanceof Long) {
                    preparedStatement.setLong(index + 1, (Long) value);
                } else if (value instanceof Float) {
                    preparedStatement.setFloat(index + 1, (Float) value);
                } else if (value instanceof Double) {
                    preparedStatement.setDouble(index + 1, (Double) value);
                } else if (value instanceof Timestamp) {
                    preparedStatement.setTimestamp(index + 1, (Timestamp) value);
                } else if (value instanceof Date) {
                    preparedStatement.setDate(index + 1, (Date) value);
                } else if (value instanceof Blob) {
                    preparedStatement.setBlob(index + 1, (Blob) value);
                } else if (value instanceof Boolean) {
                    preparedStatement.setBoolean(index + 1, (Boolean) value);
                } else {
                    preparedStatement.setObject(index + 1, value);
                }
            }
        }
    }

    public static Long getSequence(String databaseType, String dbName, String processName, String requestId, String sequence) {
        try {
            return executeQuery(databaseType, dbName, processName, requestId, "select nextval('" + sequence + "') as nextval", null, resultSet -> {
                try {
                    if (resultSet.next()) {
                        return resultSet.getLong("nextval");
                    }
                } catch (Exception e) {
                }
                return null;
            });
        } catch (Exception e) {
            return null;
        }
    }

    public static class Result<T> extends ArrayList<T> {
        private List<T> list = new ArrayList<T>();

        public List<T> getList() {
            return list;
        }

        public T getFirst() {
            if (null != list && !list.isEmpty()) {
                return list.get(0);
            }
            return null;
        }
    }
}