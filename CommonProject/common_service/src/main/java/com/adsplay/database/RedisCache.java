package com.adsplay.database;

import com.adsplay.common.logs.WriteLog;
import com.adsplay.conf.RedisConfig;

import java.util.List;

public class RedisCache {

    private static RedisCache INSTANCE = new RedisCache();

    public static RedisCache getInstance() {
        return INSTANCE;
    }

    private static Redis RETARGETING_REDIS;
    private static Redis DEBUGGER_CLIENT_REDIS;
    private static Redis NORMAL_CASE_REDIS;
    private static Redis EVENT_REDIS;
    private static Redis DELIVERY_CACHE_REDIS;

    public static void init(String processName, List<RedisConfig> redisConfig) {
        // Loop redis config and return the related Redis Client with host,port
        // *** Need to provide some new sources here ***
        redisConfig.forEach((redis) -> {
            switch (redis.getAdsConst(redis.getName())) {
            case DEBUGGER_CLIENT_SOURCE:
                WriteLog.write(redis.getName(), "REDIS", "Start Redis Host: " + redis.getHost() + " - Port: " + redis.getPort());
                DEBUGGER_CLIENT_REDIS = new Redis(redis.getHost(), redis.getPort(), redis.getPoolSize());
                break;
            case RETARGETING_SOURCE:
                WriteLog.write(redis.getName(), "REDIS", "Start Redis Host: " + redis.getHost() + " - Port: " + redis.getPort());
                RETARGETING_REDIS = new Redis(redis.getHost(), redis.getPort(), redis.getPoolSize());
                break;
            case EVENT_SOURCE:
                WriteLog.write(redis.getName(), "REDIS", "Start Redis Host: " + redis.getHost() + " - Port: " + redis.getPort());
                EVENT_REDIS = new Redis(redis.getHost(), redis.getPort(), redis.getPoolSize());
                break;
            case NORMAL_CASE_SOURCE:
                WriteLog.write(redis.getName(), "REDIS", "Start Redis Host: " + redis.getHost() + " - Port: " + redis.getPort());
                NORMAL_CASE_REDIS = new Redis(redis.getHost(), redis.getPort(), redis.getPoolSize());
                break;
            case DELIVERY_CACHE_REDIS:
                WriteLog.write(redis.getName(), "REDIS", "Start Redis Host: " + redis.getHost() + " - Port: " + redis.getPort());
                DELIVERY_CACHE_REDIS = new Redis(redis.getHost(), redis.getPort(), redis.getPoolSize());
                break;
            default:
                break;
            }
        });

    }

    public Redis getEventRedis() {
        return EVENT_REDIS;
    }

    public Redis getNormalCaseRedis() {
        return NORMAL_CASE_REDIS;
    }

    public Redis getRetargetingRedis() {
        return RETARGETING_REDIS;
    }

    public Redis getDebuggerRedis() {
        return DEBUGGER_CLIENT_REDIS;
    }

    public Redis getDeliveryRedisCache() {
        return DELIVERY_CACHE_REDIS;
    }
}
