package com.adsplay.database;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.conf.KafkaConfig;
import com.adsplay.conf.TopicsConfig;
import com.adsplay.service.DynamicClassFactory;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.processor.ProcessorSupplier;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;

import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * User: khoivu
 * Date: 5/14/18
 * Time: 10:24 AM
 */
public class KafkaService {

    private static ConcurrentHashMap<String, KafkaStreams> cacheStream = new ConcurrentHashMap<>();

    private static KafkaService INSTANCE = new KafkaService();

    private static final ThreadLocal<Random> random = ThreadLocal.withInitial(Random::new);

    private static Producer<String, String> producer;

    public static KafkaService getInstance() {
        return INSTANCE;
    }

    public void init(KafkaConfig kafkaConfigs, String projectName) throws Exception {
        List<TopicsConfig> topicsConfigs = DataRepository.getKafkaTopic(
                projectName,
                UUID.randomUUID().toString()
        );
        topicsConfigs.stream().filter(TopicsConfig::isActive).forEach(topic -> createProcess(topic, kafkaConfigs));
        createProducer(kafkaConfigs);
        /*cacheStream.forEach((k, streams) -> {
            streams.start();
            MultiLog.write(k, "KAFKA", "CREATE KAFKA: " + k + "...DONE!!");
            Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
        });*/
    }

    private void createProcess(TopicsConfig topicsConfig, KafkaConfig kafkaConfigs) {
        MultiLog.write(topicsConfig.getGroupId(), "KAFKA", "---------------------------------------");
        MultiLog.write(topicsConfig.getGroupId(), "KAFKA", "Server [" + kafkaConfigs.getServer() + "], Topic [" + topicsConfig.getName() + "]");
        MultiLog.write(topicsConfig.getGroupId(), "KAFKA", "----------------------------------------");
        deployKafka(topicsConfig, kafkaConfigs);
        MultiLog.write(topicsConfig.getGroupId(), "KAFKA", "CREATE KAFKA: " + topicsConfig.getName() + "...DONE!!");
    }

    private void createProducer(KafkaConfig kafkaConfig) {
        Properties config = new Properties();
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getServer());
        config.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
        config.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1000);
        config.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, 10000);
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Long().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put("key.serializer", StringSerializer.class.getName());
        config.put("value.serializer", StringSerializer.class.getName());
        producer = new KafkaProducer<>(config);
    }

    private void deployKafka(TopicsConfig topicsConfig, KafkaConfig kafkaConfig) {
        Properties config = new Properties();
        config.put(ConsumerConfig.GROUP_ID_CONFIG, topicsConfig.getGroupId());
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, topicsConfig.getAppId());
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfig.getServer());
        config.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
        config.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 1000);
        config.put(StreamsConfig.BUFFERED_RECORDS_PER_PARTITION_CONFIG, 10000);
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        /*if (0 < topicsConfig.getNumberOfProcessors()) {
            config.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, topicsConfig.getNumberOfProcessors());
        }*/
        StreamsBuilder builder = new StreamsBuilder();
        Topology topology = builder.build();
        topology.addSource(topicsConfig.getSourceName(), topicsConfig.getName());
        topology.addProcessor(
                topicsConfig.getProcessorName(),
                DynamicClassFactory.getInstance().createObject(
                        topicsConfig.getExecuteClass(),
                        ProcessorSupplier.class,
                        topicsConfig.getSourceName(),
                        kafkaConfig,
                        topicsConfig
                ),
                topicsConfig.getSourceName());
        KafkaStreams streams = new KafkaStreams(topology, config);
        streams.setUncaughtExceptionHandler((Thread thread, Throwable throwable) -> MultiLog.error(topicsConfig.getGroupId(), topicsConfig.getName(), "ERROR CREATE KAFKA THREAD_NAME: " + thread.getName(), throwable));
        cacheStream.put(topicsConfig.getName(), streams);
        streams.start();
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    public void send(String topic, String key, String value) {
        int sizeRandom = producer.partitionsFor(topic).size();
        producer.send(new ProducerRecord<>(topic, random.get().nextInt(sizeRandom), key, value));
    }

    public boolean stopStreams(String name) throws Exception {
        KafkaStreams streams = cacheStream.get(name);
        if (streams != null) {
            streams.close();
            cacheStream.remove(name);
            return true;
        } else {
            MultiLog.write(name, "KAFKA", "Error KafkaStreams is NULL!!!");
            return false;
        }
    }
}
