package com.adsplay.database;

import com.adsplay.common.util.Utils;
import com.adsplay.conf.DatabaseConfig;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * User: khoivu
 * Date: 8/25/18
 * Time: 3:23 PM
 */
public abstract class ConnectionFactory {

    public abstract void init(String processName, DatabaseConfig config);

    public Connection getConnection() throws SQLException {
        return getConnection("DATABASE", System.currentTimeMillis() + "");
    }

    public Connection getConnection(String dbName) throws SQLException {
        return getConnection("DATABASE", System.currentTimeMillis() + "", dbName);
    }

    public abstract Connection getConnection(String serviceCode, String requestId) throws SQLException;

    public abstract Connection getConnection(String serviceCode, String requestId, String dbName) throws SQLException;

    public static void close(Connection conn) {
        try {
            Utils.close(conn);
            conn = null;
        } catch (Exception ex) {
            conn = null;
        }

    }

    public static boolean checkErrorMessage(String message) {
        return message != null && (message.contains("connection is closed")
                                   || message.contains("statement is closed")
                                   || message.contains("ResultSet is closed")
                                   || message.contains("TNS:bad packet")
                                   || message.contains("IO Error: Socket closed"));
    }

}
