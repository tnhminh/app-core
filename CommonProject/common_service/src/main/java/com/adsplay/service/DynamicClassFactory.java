package com.adsplay.service;

import com.adsplay.jcl.JarLoader;
import org.xeustechnologies.jcl.JclObjectFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author khoivu
 */
public class DynamicClassFactory {

    private static final DynamicClassFactory INSTANCE = new DynamicClassFactory();

    public static DynamicClassFactory getInstance() {
        return INSTANCE;
    }

    private JarLoader jarClassLoader;
    private final Map<String, String> jarsFiles = new HashMap<>();

    private DynamicClassFactory() {
        jarClassLoader = new JarLoader();
    }

    public void addJar(String fullPathJarFile, String moduleName, boolean reload) {
        if (reload) {
            reload(fullPathJarFile);
        } else {
            jarsFiles.put(moduleName, fullPathJarFile);
            jarClassLoader.add(fullPathJarFile);
        }
    }

    public void reloadAll() {
        jarClassLoader = new JarLoader(jarsFiles.values());
    }

    public void reload(String jarFile) {
        synchronized (jarClassLoader) {
            jarClassLoader.reloadJar(jarFile);
        }
    }

    public void reset() {
        jarClassLoader = new JarLoader();
        jarsFiles.clear();

    }

    public <T> T createObject(String className, Class<T> type, Object... objects) {
        return (T) JclObjectFactory.getInstance()
                .create(jarClassLoader, className, objects);
    }

}
