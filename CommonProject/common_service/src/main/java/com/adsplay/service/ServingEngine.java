package com.adsplay.service;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.DatabaseConfig;
import com.adsplay.database.DataRepository;
import com.adsplay.entity.*;
import com.adsplay.model.CampaignModel;
import com.adsplay.model.CategoryModel;
import com.adsplay.model.ContentModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * User: khoivu
 * Date: 8/29/18
 * Time: 2:53 PM
 */
public class ServingEngine {

    private HashMap<Integer, Campaign> cacheCampaign = new HashMap<>();

    private HashMap<Integer, Regions> cacheRegions = new HashMap<>();

    private HashMap<String, Provinces> cacheProvinces = new HashMap<>();

    private HashMap<Integer, Placement> cachePlacement = new HashMap<>();

    private HashMap<String, CategoryModel> cacheCategories = new HashMap<>();

    private HashMap<String, ContentModel> cacheContent = new HashMap<>();

    private List<CategoryModel> cacheCategoriesList = new ArrayList<>();

    private List<ContentModel> cacheContentList = new ArrayList<>();

    private HashMap<Integer, Flight> cacheFlight = new HashMap<>();

    private HashMap<Integer, Creatives> cacheCreative = new HashMap<>();

    private HashMap<Integer, AccessPoint> cacheAccessPoints = new HashMap<>();

    private HashMap<Integer, Accounts> cachePublisher = new HashMap<>();

    private HashMap<String, Zone> cacheZoneByName = new HashMap<>();

    private HashMap<String, Place> cachePlaceByName = new HashMap<>();

    private HashMap<String, Object> cacheGatewayReport = new HashMap<>();

    private HashMap<String, List<CampaignModel>> cacheCampaignModel = new HashMap<>();

    private static ServingEngine instance = new ServingEngine();

    private static final String PROCESS_NAME = "INIT";

    protected void init(long longExpired, long mediumExpired, long shortExpired) {
        
    }

    public static ServingEngine getInstance() {
        return instance;
    }

    public String getCampaignName(int id) {
        if (id < 1) {
            return "Other";
        }
        Campaign campaign = cacheCampaign.get(id);
        return null == campaign ? "Other" : campaign.getName();
    }

    public CategoryModel getCategoryByPublisherCode(String publiserCode) {
        List<CategoryModel> collect = cacheCategoriesList.stream().filter(category -> category.getPublisherCode().equals(publiserCode)).collect(Collectors.toList());
        return collect.isEmpty() ? null : collect.get(0);
    }

    public ContentModel getContentByPublisherCode(String publisherCode) {
        List<ContentModel> collect = cacheContentList.stream().filter(content -> content.getPublisherCode().equals(publisherCode)).collect(Collectors.toList());
        return collect.isEmpty() ? null : collect.get(0);
    }

    public String getContentName(String publisherCode) {
        if (!Utils.isEmpty(publisherCode)) {
            ContentModel contentModel = cacheContent.get(publisherCode);
            return null == contentModel ? "Other" : contentModel.getName();
        } else {
            return "Other";
        }
    }

    public String getCategoryName(String id) {
        if (!Utils.isEmpty(id)) {
            CategoryModel categoryModel = cacheCategories.get(id);
            return null == categoryModel ? "Other" : categoryModel.getName();
        } else {
            return "Other";
        }
    }

    public String getPlacementName(int id) {
        if (id < 1) {
            return "Other";
        }
        Placement placement = cachePlacement.get(id);
        return null == placement ? "Other" : placement.getName();
    }

    public String getRegionName(int regionId) {
        if (regionId <= 0) {
            return "Other";
        }
        Regions regions = cacheRegions.get(regionId);
        return null == regions ? "Other" : Utils.replaceVnChars(regions.getName());
    }

    public String getProvinceName(String publisherCode) {
        if (Utils.isEmpty(publisherCode)) {
            return "Other";
        }
        Provinces provinces = cacheProvinces.get(publisherCode);
        return null == provinces ? "Other" : Utils.replaceVnChars(provinces.getName());
    }

    public String getProvinceId(String provinceName) {
        if (Utils.isEmpty(provinceName)) {
            return "Other";
        }
        String provinceId = null;
        for (String key : cacheProvinces.keySet()) {
            Provinces provinces = cacheProvinces.get(key);
            if (provinceName.trim().equals(Utils.replaceVnChars(provinces.getName()).trim())) {
                provinceId = key;
                break;
            }
        }
        return null == provinceId ? "Other" : provinceId;
    }

    public String getFlightName(int id) {
        if (id <= 0) {
            return "";
        }
        Flight flight = cacheFlight.get(id);
        return null == flight ? "" : flight.getName();
    }

    public AccessPoint getAPById(String id) {
        if (Utils.isEmpty(id) || !Utils.isNumeric(id)) {
            return null;
        }
        return cacheAccessPoints.get(Integer.parseInt(id));
    }

    public Accounts getPublisherById(int id) {
        if (id < 1) {
            return null;
        }
        return cachePublisher.get(id);
    }

    public String getPublisherNameById(int id) {
        if (id < 1) {
            return "OTHER";
        }
        return null == cachePublisher.get(id) ? "OTHER" : cachePublisher.get(id).getName();
    }

    public Zone getZoneByName(String name) {
        if (Utils.isEmpty(name) || name.equals("-")) {
            return null;
        }
        return cacheZoneByName.get(name);
    }

    public Place getPlaceByName(String name) {
        if (Utils.isEmpty(name) || name.equals("-")) {
            return null;
        }
        return cachePlaceByName.get(name);
    }

    public Object getResultGatewayReport(String key) {
        return cacheGatewayReport.get(key);
    }

    public Object setResultGatewayReport(String key, Object o) {
        return cacheGatewayReport.put(key, o);
    }


    public String getCreativeName(int id) {
        if (id <= 0) {
            return "Other";
        }
        Creatives creative = cacheCreative.get(id);
        return null == creative ? "Other" : creative.getName();
    }
}
