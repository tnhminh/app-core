package com.adsplay.service;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.JsonUtil;
import com.adsplay.database.RedisCache;
import com.adsplay.model.wifi.WifiLocation;

import java.util.HashMap;
import java.util.List;

/**
 * User: khoivu
 * Date: 4/27/18
 * Time: 2:39 PM
 */
public class ParserLog {

    public static HashMap<String, String> parserRequestParams(String processName, String regex, String offset, String uri) {
        MultiLog.write(processName, offset, "URI: " + uri);
        HashMap<String, String> requestParams = new HashMap<>();
        if (!Utils.isEmpty(uri)) {
            List<String> params = Utils.subParams(regex, uri);
            params.forEach(param -> {
                String[] split = param.split("=");
                requestParams.put(split[0], Utils.nullToEmpty(split[1]));
            });
        }
        return requestParams;

    }

    /*public static HashMap<String, String> parserRequestParams(DefaultRequest request) {
        HashMap<String, String> requestParams = new HashMap<>();
        requestParams.put(AdsConst.PARAM_PLACEMENT_ID, Utils.nullToEmpty(request.getRequests().getArgPlacementId()));
        requestParams.put(AdsConst.PARAM_CONTENT_ID, Utils.nullToEmpty(ParserRequest.getContentId(request)));
        requestParams.put(AdsConst.PARAM_CAMPAIGN_ID, Utils.nullToEmpty(request.getRequests().getArgCampaignId()));
        requestParams.put(AdsConst.PARAM_FLIGHT_ID, Utils.nullToEmpty(request.getRequests().getArgFlightId()));
        requestParams.put(AdsConst.PARAM_BUY_TYPE, Utils.nullToEmpty(request.getRequests().getArgBuyType()));
        requestParams.put(AdsConst.PARAM_AD_ID, Utils.nullToEmpty(request.getRequests().getArgAdId()));
        requestParams.put(AdsConst.PARAM_T, Utils.nullToEmpty(request.getRequests().getArgT()));
        requestParams.put(AdsConst.PARAM_EVENT, Utils.nullToEmpty(request.getRequests().getArgEvent()));
        requestParams.put(AdsConst.PARAM_UUID, Utils.nullToEmpty(request.getRequests().getArgUuid()));
        requestParams.put(AdsConst.PARAM_CAT_ID, Utils.nullToEmpty(request.getRequests().getArgCategoryId()));
        requestParams.put(AdsConst.PARAM_PROVINCE_ID, Utils.nullToEmpty(request.getRequests().getArgProvinceId()));
        requestParams.put(AdsConst.PARAM_CHANNEL_ID, Utils.nullToEmpty(request.getRequests().getArgChannelId()));
        requestParams.put(AdsConst.PARAM_CHANNEL_NAME, Utils.nullToEmpty(request.getRequests().getArgChannelName()));
        requestParams.put(AdsConst.PARAM_TIME_PUSH_EVENT, Utils.nullToEmpty(request.getRequests().getArgTimePushEvent()));
        requestParams.put(AdsConst.PARAM_AD_TYPE, Utils.nullToEmpty(request.getRequests().getArgAdType()));
        requestParams.put(AdsConst.PARAM_MAC_ADDRESS, Utils.nullToEmpty(request.getRequests().getArgAPMacAddress()));
        return requestParams;
    }

    public static IPLocation parserLocation(String processName, String offset, String redisPrefix, long ip) throws IOException {
        IPLocation ipLocation = new IPLocation();
        String ipLocationCache = RedisCache.getInstance().getIpLocation().getCache(
                processName,
                offset,
                redisPrefix + ip
        );
        if (!Utils.isEmpty(ipLocationCache)) {
            List<String> ipArray = JsonUtil.fromString(ArrayList.class, ipLocationCache);
            ipLocation.setCountry(ipArray.get(0));
            ipLocation.setProvinceName(ipArray.get(1));
            if (!Utils.isEmpty(ipLocation.getProvinceName())) {
                try {
                    ipLocation.setProvinceId(Utils.nullToEmpty(CoreConfig.getInstance().getProvince(Utils.nullToEmpty(ipLocation.getProvinceName()))));
                } catch (Exception e) {
                    MultiLog.error(processName, offset, ip, "Error cannot parser IP location with Province: [" + Utils.nullToEmpty(ipLocation.getProvinceName()) + "]", e);
                }
            }
        }
        return ipLocation;
    }*/

    /*public static DateTimeReport parserTime(String processName, String offset, Date receiveTime) {
        DateTimeReport dateTimeReport = new DateTimeReport();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(receiveTime);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);

            dateTimeReport.setTime(receiveTime.getTime());
            dateTimeReport.setDate(getInstance().formatDate(receiveTime));
            dateTimeReport.setHour(getInstance().formatHour(receiveTime));
            dateTimeReport.setDateTime(getInstance().formatDateTime(receiveTime));

        } catch (ParseException e) {
            MultiLog.error(processName, offset, "Error cannot convert receive time!!!", e);
        }
        return dateTimeReport;
    }*/

    public static WifiLocation getWifiLocation(String processName, String requestId, String prefixWifiLocation, String ap_mac_address) {
        String wifiCache = RedisCache.getInstance().getRetargetingRedis().getCache(
                processName,
                requestId,
                prefixWifiLocation + ap_mac_address.replaceAll(":", "")
        );
        if (!Utils.isEmpty(wifiCache)) {
            try {
                return JsonUtil.fromString(WifiLocation.class, wifiCache);
            } catch (Exception e) {
                MultiLog.error(processName, requestId, ap_mac_address, "Error cannot get wifi location cache with this key " + prefixWifiLocation + ap_mac_address, e);
            }
        }
        return new WifiLocation();
    }

}
