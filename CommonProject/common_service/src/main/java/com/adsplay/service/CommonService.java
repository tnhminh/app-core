package com.adsplay.service;

import java.util.List;

import com.adsplay.conf.CoreConfig;
import com.adsplay.conf.DatabaseConfig;
import com.adsplay.conf.RedisConfig;
import com.adsplay.database.RedisCache;

/**
 * User: khoivu
 * Date: 8/26/18
 * Time: 11:19 PM
 */
public class CommonService {

    public static void init(String processName, CoreConfig coreConfig, DatabaseConfig clickhouseConfig, DatabaseConfig postgresConfig, List<RedisConfig> redisConfig) throws Exception {
        if (null != redisConfig) {
            RedisCache.init(null, redisConfig);
        }
    }

}
