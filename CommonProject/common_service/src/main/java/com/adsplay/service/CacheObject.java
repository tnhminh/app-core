package com.adsplay.service;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.concurrent.TimeUnit;

/**
 * User: khoivu
 * Date: 8/29/18
 * Time: 10:48 AM
 */
public class CacheObject {

    private Cache<String, Object> cache;
    private static CacheObject cacheObject;

    public static CacheObject getInstance() {
        return cacheObject != null ? cacheObject : new CacheObject(5, 5, 10000);
    }

    public CacheObject(long expiredTime, long accessTime, long maxSize) {
        this.cache = Caffeine.newBuilder()
                .expireAfterAccess(accessTime, TimeUnit.MINUTES)
                .expireAfterWrite(expiredTime, TimeUnit.MINUTES)
                .maximumSize(maxSize)
                .build();
    }

    public void add(String key, Object value) {
        cache.put(key, value);
    }

    public Object get(String key) {
        return cache.getIfPresent(key);
    }

    public void remove(String key) {
        cache.invalidate(key);
    }

}
