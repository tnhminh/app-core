package com.adsplay.common.util;

import com.adsplay.Constant;
import com.adsplay.model.UserAgentModel;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;

import java.util.HashMap;
import java.util.Map;

/**
 * User: khoivu
 * Date: 5/8/18
 * Time: 3:04 PM
 */
public class UserAgentUtils {

    private static UserAgentUtils INSTANCE = new UserAgentUtils();
    private static UserAgentAnalyzer agentAnalyzer;

    public static UserAgentUtils getInstance() {
        return INSTANCE;
    }

    public static void init() {
        agentAnalyzer = (UserAgentAnalyzer) UserAgentAnalyzer
                .newBuilder()
                .hideMatcherLoadStats()
                .withCache(25000)
                .withField(Constant.AGENT_DEVICE_CLASS)
                .withField(Constant.AGENT_SYSTEM_NAME)
                .withField(Constant.AGENT_SYSTEM_NAME_VERSION)
                .withField(Constant.AGENT_DEVICE_BRAND)
                .withField(Constant.AGENT_DEVICE_VERSION)
                .withField(Constant.AGENT_DEVICE_NAME)
                .withField(Constant.AGENT_NAME_VERSION)
                .withField(Constant.AGENT_OS_VERSION_BUILD)
                .build();
    }

    public UserAgentModel parser(String processName, String requestId, String data) {
        if (Utils.isEmpty(data)) {
//            WriteLog.write(processName, requestId, "USER_AGENT is NULL!!!");
            return null;
        }
        UserAgentModel userAgent = new UserAgentModel();
        UserAgent agent = agentAnalyzer.parse(data);
        Map<String, String> dataParsing = parsingUserAgent(agent, agent.getUserAgentString());
        String appType = agent.get(Constant.AGENT_SYSTEM_NAME).getValue();
        if (appType.toLowerCase().equals(Constant.DEVICE_ANDROID) ||
                appType.toLowerCase().equals(Constant.DEVICE_IOS) ||
                appType.toLowerCase().equals(Constant.DEVICE_MAC_OS)
        ) {
            userAgent.setDeviceAppType(appType);
            userAgent.setDeviceOsVersion(agent.get(Constant.AGENT_DEVICE_VERSION).getValue());
        } else {
            userAgent.setDeviceAppType(agent.get(Constant.AGENT_SYSTEM_NAME_VERSION).getValue());
        }
        userAgent.setDeviceName(dataParsing.get("deviceName"));
        userAgent.setBrandName(dataParsing.get("brandName"));
        userAgent.setUserAgent(agent.getUserAgentString());
        try {
            userAgent.setOsVersionBuild(Utils.nullToEmpty(agent.get(Constant.AGENT_OS_VERSION_BUILD).getValue()));
        } catch (Exception e) {
            userAgent.setOsVersionBuild("");
        }
//        for (String fieldName : agent.getAvailableFieldNamesSorted()) {
//            System.out.println(fieldName + " = " + agent.getValue(fieldName));
//        }
//        WriteLog.write(processName, requestId, "USER_AGENT: [" + JsonUtils.toJson(userAgent) + "]");
        return userAgent;
    }

    public Map<String, String> parsingUserAgent(UserAgent agent, String userAgent) {
        String deviceName;
        String brandName;
        String deviceClass;
        String space = " ";
        brandName = agent.getValue(Constant.AGENT_DEVICE_BRAND);
        deviceClass = agent.getValue(Constant.AGENT_DEVICE_CLASS);
        deviceName = agent.getValue(Constant.AGENT_DEVICE_NAME);
        userAgent = userAgent.toUpperCase();
        boolean isSonyInternetTV = Utils.isTvType(userAgent, new String[]{"SONY-"});
        boolean isSonyAndroidTV = Utils.isTvType(userAgent, new String[]{"BRAVIA"});
        boolean isToshibaTV = Utils.isTvType(userAgent, new String[]{"TOSHIBA-"});
        boolean isLGTV = Utils.isTvType(userAgent, new String[]{"LGE"});
        boolean isAsanzoTV = Utils.isTvType(userAgent, new String[]{"ASANZO TV"});
        boolean isPanasonicVeria = Utils.isTvType(userAgent, new String[]{"SMARTTV", "VIERA"});
        boolean isDesktop = deviceClass.toLowerCase().equals("desktop");
        if (isSonyInternetTV || isSonyAndroidTV || isToshibaTV || isLGTV || isAsanzoTV || isPanasonicVeria || isDesktop) {
            if (isDesktop) {
                deviceName = agent.getValue(Constant.AGENT_SYSTEM_NAME) + space + agent.getValue(Constant.AGENT_NAME_VERSION);
                if (brandName.toLowerCase().equals("unknown")) {
                    brandName = "PC";
                }
            }
            if (isSonyInternetTV) {
                String[] token = userAgent.substring(userAgent.indexOf("SONY-")).split(space);
                deviceName = token[0];
                brandName = "SONY";
            }
            if (isSonyAndroidTV) {
                deviceName = "Sony" + space + deviceName;
                brandName = "SONY";
            }
            if (isToshibaTV) {
                String[] token = userAgent.substring(userAgent.indexOf("TOSHIBA-")).split(space);
                deviceName = token[0].toUpperCase();
                brandName = "TOSHIBA";
            }
            if (isLGTV) {
                String[] token = userAgent.substring(userAgent.lastIndexOf("(LGE,")).replaceAll("[(),]", "").split(space);
                deviceName = token[0] + space + token[1];
                brandName = "LGE";
            }
            if (isAsanzoTV) {
                brandName = "ASANZO";
            }
            if (isPanasonicVeria) {
                deviceName = "PANASONIC VERIA TV";
                brandName = "PANASONIC";
            }
        }
        Map<String, String> data = new HashMap<>();
        data.put("deviceName", deviceName);
        data.put("brandName", brandName);
        return data;
    }


    public static void main(String[] args) {
        init();
        getInstance().parser(
                "Test",
                "1234566",
                "Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16A366"
        );
    }

}
