package com.adsplay.common.logs;

import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.graylog2.gelfclient.GelfConfiguration;
import org.graylog2.gelfclient.GelfMessage;
import org.graylog2.gelfclient.GelfMessageBuilder;
import org.graylog2.gelfclient.GelfMessageLevel;
import org.graylog2.gelfclient.GelfTransports;
import org.graylog2.gelfclient.transport.GelfTransport;

import com.adsplay.Constant;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.GrayLogConfig;
import com.adsplay.conf.JsonUtil;

import io.sentry.Sentry;
import io.sentry.event.Breadcrumb;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.User;
import io.sentry.event.UserBuilder;

/**
 * @author khoivu
 */
class ProxyLog {

    private final Logger log;

    private static GelfConfiguration gelfConf;

    private static GelfTransport transport;

    private static GelfMessageBuilder builder;

    private static Breadcrumb breadcrumb;

    private static User userBuilder;

    private static boolean isActiveSentry;

    private static boolean isActiveGrayLog;

    protected ProxyLog(String folderPath, Level level, String name, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig, String currentDate, String currentHour) {
        if (!Utils.isEmpty(sentryDns) && !Utils.isEmpty(sentryEmail)) {
            //Sentry log
            initSentry(sentryDns, sentryEmail);
            isActiveSentry = true;
        } else {
            isActiveSentry = false;
        }

        //Graylog
        if (null != grayLogConfig && grayLogConfig.isActive()) {
            initGrayLog(grayLogConfig);
            isActiveGrayLog = true;
        } else {
            isActiveGrayLog = false;
        }

        String cDay = currentDate.isEmpty() ? "" : "/" + currentDate;
        String cHour = currentHour.isEmpty() ? "" : "/" + currentHour;
        String logName = Utils.isEmpty(name) ? ProxyLog.class.getSimpleName() : name + cDay + cHour;
        log = LogManager.getLogger(logName);
        if (Utils.isEmpty(folderPath)) {
            System.err.println("Could not set log dir[" + folderPath + "]");
        } else {
            String filePath = folderPath + "/" + logName + ".log";
            org.apache.logging.log4j.core.Logger coreLogger = (org.apache.logging.log4j.core.Logger) log;
            if (!coreLogger.getAppenders().containsKey(logName)) {
                coreLogger.addAppender(createFileAppender(logName, filePath));
                coreLogger.setLevel(level);
                loggerStart();
            }
        }
    }

    private void loggerStart() {
        String time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
        log.info("####################################################################");
        log.info("#                                                                  #");
        log.info("#                             EXECUTE                              #");
        log.info("#                  Time Start : " + time + "                #");
        log.info("#                                                                  #");
        log.info("####################################################################");
    }

    private static Appender createFileAppender(String name, String filePath) {
        LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
        Configuration config = ctx.getConfiguration();
        PatternLayout layout = PatternLayout.newBuilder()
                                            .withConfiguration(config)
                                            .withPattern("%d [%t] %-5level: %msg%n%throwable")
                                            .build();

        // TriggeringPolicy policy = CompositeTriggeringPolicy.createPolicy(
        // SizeBasedTriggeringPolicy.createPolicy("500 MB"),
        // TimeBasedTriggeringPolicy.newBuilder().withInterval(1).build()
        // );

        Appender appender = FileAppender.newBuilder()
                                               .setConfiguration(config)
                                               .withName(name)
                                               .withLayout(layout)
                                               .withFileName(filePath)
//                                               .withPolicy(policy)
                                               .build();
        appender.start();
        config.addAppender(appender);
        return appender;
    }

    private void initSentry(String sentryDns, String sentryEmail) {
        Sentry.init(sentryDns);
        userBuilder = new UserBuilder().setEmail(sentryEmail).build();
        breadcrumb = new BreadcrumbBuilder()
                .setTimestamp(new Date())
                .setLevel(Breadcrumb.Level.ERROR)
                .setCategory(Constant.SENTRY_BREADCRUMB_CATEGORY)
                .setMessage(Constant.SENTRY_BREADCRUMB_MESSAGE)
                .build();
        Runtime.getRuntime().addShutdownHook(new Thread(Sentry::close));
    }

    public void sentryError(Object processName, Object message, Throwable e) {
        if (!isActiveSentry) {
            return;
        }
        if (null != breadcrumb) {
            try {
                // Record a breadcrumb in the current context. By default the last 100 breadcrumbs are kept.
                Sentry.getContext().recordBreadcrumb(breadcrumb);
                // Set the user in the current context.
                Sentry.getContext().setUser(userBuilder);
                if (!Utils.isEmpty(processName)) {
                    Sentry.getContext().addTag(Constant.LOG_PROCESS_NAME, processName.toString());
                }
                //        if (!Utils.isEmpty(receiveData)) Sentry.getContext().addTag(Constant.LOG_RECEIVE_DATA, receiveData.toString());
                if (!Utils.isEmpty(message)) {
                    Sentry.getContext().addTag(Constant.SENTRY_TAG_MESSAGE, message.toString());
                }
                //        if (!Utils.isEmpty(requestId)) Sentry.getContext().addTag(Constant.LOG_REQUEST_ID, requestId.toString());
                Sentry.capture(e);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void initGrayLog(GrayLogConfig grayLogConfig) {
        gelfConf = new GelfConfiguration(new InetSocketAddress(grayLogConfig.getHost(), grayLogConfig.getPort()))
                .transport(GelfTransports.UDP)
                .queueSize(grayLogConfig.getQueueSize())
                .connectTimeout(grayLogConfig.getTimeout())
                .reconnectDelay(grayLogConfig.getReconnectDelay())
                .sendBufferSize(grayLogConfig.getBufferSize());

        transport = GelfTransports.create(gelfConf);
        builder = new GelfMessageBuilder(grayLogConfig.getMessage(), grayLogConfig.getSource());
        Runtime.getRuntime().addShutdownHook(new Thread(transport::stop));
    }

    public void sendInfo(Object processName, String jsonData) {
        if (!isActiveGrayLog) {
            return;
        }
        try {
            Map<String, Object> additional = new HashMap<>();
            Map<String, Object> extras = JsonUtil.fromString(Map.class, jsonData);
            extras.forEach((k, v) -> {
                if (v instanceof Map) {
                    ((Map) v).forEach((x, y) -> additional.put(x.toString(), y));
                } else {
                    additional.put(k, v);
                }
            });
            additional.put(Constant.LOG_PROCESS_NAME, processName);
            final GelfMessage message = builder.message(jsonData)
                                               .level(GelfMessageLevel.INFO)
                                               .additionalFields(additional)
                                               .timestamp(System.currentTimeMillis())
                                               .build();
            transport.send(message);
        } catch (Exception e) {
            sentryError(processName, jsonData, e);
        }
    }

    public void debug(Object message) {
        log.debug(message);
    }

    public void debug(Object transid, Object object) {
        debug("[" + transid + "]---" + object);
    }

    public void debug(Object workerName, Object transid, Object object) {
        debug("[" + workerName + "]" + "[" + transid + "]", object);
    }

    public void debug(Object transId, Object object, Object phoneNumber, Object message) {
        debug(String.format("[%s][%s]", transId, object), message);
    }

    public void debug(Object transId, Object object, Object phoneNumber, String process, Object message) {
        debug(String.format("[%s][%s][%s]", transId, object, process), message);
    }

    public void error(Object object, Throwable e) {
        log.error(object, e);
    }

    public void error(Throwable e) {
        error(e.getMessage(), e);
    }

    public void error(Object transId, Object object, Throwable e) {
        error(Utils.concatString("[", transId, "]---", object), e);
    }

    public void error(Object transId, Object object, String message, Throwable e) {
        error(Utils.concatString("[", transId, "][", object, "]"), message, e);
    }

    public void error(Object transId, Object object, String process, String message, Throwable e) {
        error(Utils.concatString("[", transId, "][", object, "][", process, "]"), message, e);
    }

    public void write(Object object) {
        log.info(object);
    }

    public void write(Object transid, Object... object) {
        write(Utils.concatString("[", transid, "]---", object));
    }

    public void write(Object transId, String process, String content) {
        write(Utils.concatString("[", transId, "][", process, "]---", content));
    }

    public void write(Object transId, String object, String process, Object... objects) {
        write(Utils.concatString("[", transId, "][", object, "][", process, "]---", objects));
    }
}

