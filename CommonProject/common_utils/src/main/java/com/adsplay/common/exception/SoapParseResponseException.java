/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.common.exception;

/**
 *
 * @author khoivu
 */
public class SoapParseResponseException extends Throwable {

	public SoapParseResponseException(String message) {
		super(message);
	}

	public SoapParseResponseException(String message, Throwable cause) {
		super(message, cause);
	}

	public SoapParseResponseException(Throwable cause) {
		super(cause);
	}

}
