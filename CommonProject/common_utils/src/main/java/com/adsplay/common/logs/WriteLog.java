package com.adsplay.common.logs;

import com.adsplay.common.util.Utils;
import com.adsplay.conf.GrayLogConfig;
import org.apache.logging.log4j.Level;

/**
 * @author khoivu
 */
public class WriteLog {

    private static ProxyLog proxyLog;

    private static String SENTRY_DNS;

    private static String SENTRY_EMAIL;

    private static GrayLogConfig GRAYLOG_CONFIG;

    private WriteLog() {
        // NOOP
    }

    public static void startLog(String filePath, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig) {
        startLog(filePath, Level.INFO, sentryDns, sentryEmail, grayLogConfig, "", "");
    }

    public static void startLog(String filePath, Level level, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig, String curentDate, String currentHour) {
        SENTRY_DNS = sentryDns;
        SENTRY_EMAIL = sentryEmail;
        GRAYLOG_CONFIG = grayLogConfig;
        proxyLog = new ProxyLog(filePath, level, "", sentryDns, sentryEmail, grayLogConfig, "", "");
    }

    private static ProxyLog getLog() {
        if (proxyLog == null) {
            proxyLog = new ProxyLog(null, Level.INFO, "", SENTRY_DNS, SENTRY_EMAIL, GRAYLOG_CONFIG, "", "");
        }
        return proxyLog;
    }

    public static void debug(Object message) {
        getLog().debug(message);
    }

    public static void debug(Object transid, Object object) {
        debug("[" + transid + "]---" + object);
    }

    public static void debug(Object workerName, Object transid, Object object) {
        debug("[" + workerName + "]" + "[" + transid + "]", object);
    }

    public static void debug(Object transId, Object object, Object phoneNumber, Object message) {
        debug(String.format("[%s][%s]", transId, object), message);
    }

    public static void debug(Object transId, Object object, Object phoneNumber, String process, Object message) {
        debug(String.format("[%s][%s][%s]", transId, object, process), message);
    }

    public static void error(Object object, Throwable e) {
        getLog().error(object, e);
    }

    public static void errorSentry(String processName, Object object, Throwable e) {
        getLog().sentryError(processName, object, e);
    }

    public static void grayLogInfo(String processName, String jsonData) {
        getLog().sendInfo(processName, jsonData);
    }

    public static void error(Throwable e) {
        error(e.getMessage(), e);
    }

    public static void error(Object transId, Object object, Throwable e) {
        error(Utils.concatString("[", transId, "]---", object), e);
    }

    public static void error(Object transId, Object object, String message, Throwable e) {
        error(Utils.concatString("[", transId, "][", object, "]"), message, e);
    }

    public static void error(Object transId, Object object, String process, String message, Throwable e) {
        error(Utils.concatString("[", transId, "][", object, "][", process, "]"), message, e);
    }

    public static void write(Object object) {
        getLog().write(object);
    }

    public static void write(Object transid, Object object) {
        write(Utils.concatString("[", transid, "]---", object));
    }

    public static void write(Object transId, String process, Object content) {
        write(Utils.concatString("[", transId, "][", process, "]"), content);
    }

    public static void write(Object transId, Object object, String process, Object... objects) {
        write(Utils.concatString("[", transId, "][", object, "][", process, "]"), objects);
    }

}
