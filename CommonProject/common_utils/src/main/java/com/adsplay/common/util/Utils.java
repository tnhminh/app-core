package com.adsplay.common.util;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.io.Closeable;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author khoivu
 */
public class Utils {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String PIN_KEY = "debitorPin";

    public static List<NameValuePair> parserParamsURL(String url) throws URISyntaxException {
        return URLEncodedUtils.parse(new URI(url), StandardCharsets.UTF_8);
    }

    public static boolean isEmpty(Object object) {
        return object == null ? true : (object instanceof String && ((String) object).trim().isEmpty());
    }

    public static boolean isNumeric(String str) {
        if (Utils.isEmpty(str)) {
            return false;
        }
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public static List<String> subParams(String regex, String message) {
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(message);
        List<String> result = new ArrayList<>();
        while (matcher.find()) {
            result.add(matcher.group(0));
        }
        return result;
    }

    public static int convertDateToSecond(String date, String dateFormat) {
        if (date.equals("life-time")) {
            return 0;
        }
        SimpleDateFormat f = new SimpleDateFormat(dateFormat);
        try {
            Date d = f.parse(date);
            return (int) (d.getTime() / 1000);
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void close(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
            closeable = null;
        } catch (Exception ex) {
            // NOOP
        }
    }

    public static void close(AutoCloseable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
                closeable = null;
            }
        } catch (Exception ex) {
            // NOOP
        }
    }

    public static String nullToEmpty(Object object) {
        return object == null ? "" : object.toString().trim();
    }

    public static Number emptyToZero(Object object) {
        return isEmpty(object) ? 0 : new BigDecimal(object.toString());
    }

    public static String exceptionToString(Throwable ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    public static String replaceVnChars(String src) {
        if (Utils.isEmpty(src)) {
            return src;
        }
        String dest = Normalizer.normalize(src.trim(), Normalizer.Form.NFC);

        dest = dest.replaceAll("[áàãảạâấầẩẫậăắằẳẵặ]", "a").replaceAll("[óòỏõọôốồổỗộơớờởỡợ]", "o")
                .replaceAll("[íìĩỉị]", "i").replaceAll("[ýỳỷỹỵ]", "y").replaceAll("[éèẻẽẹêếềểễệ]", "e")
                .replaceAll("[úùủũụưứừửữự]", "u").replaceAll("[đ]", "d");
        dest = dest.replaceAll("[ÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬ]", "A").replaceAll("[ÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]", "O")
                .replaceAll("[ÍÌỈĨỊ]", "I").replaceAll("[ÝỲỶỸỴ]", "Y").replaceAll("[ÉÈẺẼẸÊẾỀỂỄỆ]", "E")
                .replaceAll("[ÚÙỦŨỤƯỨỪỬỮỰ]", "U").replaceAll("[Đ]", "D");
        return dest;
    }

    public static boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        return pattern.matcher(email).matches();
    }

    public static <E> E choice(Collection<? extends E> coll) {
        if (coll.isEmpty()) {
            return null; // or throw IAE, if you prefer
        }
        return choice(coll, coll.size());
    }

    public static <E> E choice(Collection<? extends E> coll, int size) {
        if (size == 1) {
            return coll.iterator().next();
        }
        Random rand = new Random();
        int index = rand.nextInt(size);
        try {
            if (coll instanceof List) { // optimization
                return ((List<? extends E>) coll).get(index);
            } else {
                Iterator<? extends E> iter = coll.iterator();
                for (int i = 0; i < index; i++) {
                    iter.next();
                }
                return iter.next();
            }
        } catch (Exception ex) {
            return null;
        }
    }

    public static String subString(String message, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }

    public static String concatString(Object... content) {
        StringBuilder builder = new StringBuilder();
        for (Object str : content) {
            if (str instanceof Object[]) {
                builder.append(concatString((Object[]) str));
            } else {
                builder.append(str);
            }
        }
        return builder.toString();
    }

    public static String template(String template, String... content) {
        if (Utils.isEmpty(template)) {
            return "";
        }
        int size = content.length;
        if (size == 0) {
            return template;
        }
        String[] datas = template.split("\\#\\d+");
        StringBuilder builder = new StringBuilder();
        int z = 0;
        if (template.matches("^\\#\\d+")) {
            builder.append(content[0]);
            z = 1;
        }
        builder.append(datas[0]);
        for (int i = 1; i < datas.length; i++) {
            builder.append(z < size ? content[z] : ("#" + i));
            builder.append(datas[i]);
            z++;
        }

        if (match(template, "\\#\\d+$")) {
            builder.append(z < size ? content[z] : ("#" + (z + 1)));
        }
        return builder.toString();
    }

    private static boolean match(String string, String regex) {
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(string);

        return matcher.find();
    }

    /**
     * Return {@code true} if any element in '{@code candidates}' is
     * contained in '{@code source}'; otherwise returns {@code false}.
     *
     * @param source     the source Collection
     * @param candidates the candidates to search for
     * @return whether any of the candidates has been found
     */
    public static boolean containsAny(Collection<?> source, Collection<?> candidates) {
        if (isEmpty(source) || isEmpty(candidates)) {
            return false;
        }
        for (Object candidate : candidates) {
            if (source.contains(candidate)) {
                return true;
            }
        }
        return false;
    }

    public static java.sql.Date convertUtilToSql(java.util.Date uDate) {
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }

    public static int getRandom(int max) {
        return (int) (Math.random() * (double) max);
    }

    public static boolean isTvType(String useragent, String[] tvType) {
        for (String s : tvType) {
            if (!useragent.contains(s)) {
                return false;
            }
        }
        return true;
    }

    public static String getStackTrace(Exception ex) {
        StringBuffer sb = new StringBuffer(500);
        StackTraceElement[] st = ex.getStackTrace();
        sb.append(ex.getClass().getName() + ": " + ex.getMessage() + "\n");
        for (int i = 0; i < st.length; i++) {
            sb.append("\t at " + st[i].toString() + "\n");
        }
        return sb.toString();
    }

    public static Date getYesterday() {
        return new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
    }
}
