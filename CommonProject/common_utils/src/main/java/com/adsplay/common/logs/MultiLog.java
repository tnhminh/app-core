package com.adsplay.common.logs;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.Level;

import com.adsplay.common.util.Utils;
import com.adsplay.conf.GrayLogConfig;

/**
 * @author khoivu
 */
public class MultiLog {

    private static final Map<String, ProxyLog> MAP = new ConcurrentHashMap<>();

    private static final String DEFAULT_CODE = "default";

    private static String parentFolder;

    private static Level logLevel;

    private static String SENTRY_DNS;

    private static String SENTRY_EMAIL;

    private static GrayLogConfig GRAYLOG_CONFIG;

    private MultiLog() {
        // NO OP
    }

    public static void setLogParentFolder(String path, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig) {
        SENTRY_DNS = sentryDns;
        SENTRY_EMAIL = sentryEmail;
        GRAYLOG_CONFIG = grayLogConfig;
        setLogParentFolder(path, false, sentryDns, sentryEmail, grayLogConfig);
    }

    public static void setLogParentFolder(String path, boolean isDebug, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig) {
        parentFolder = path;
        startLog(null, isDebug, sentryDns, sentryEmail, grayLogConfig);
    }

    private static void startLog(String requestName, boolean isDebug, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig) {
        startLog(requestName, parentFolder, isDebug ? Level.DEBUG : Level.INFO, sentryDns, sentryEmail, grayLogConfig);
    }

    public static void startLog(String requestName, String filePath, boolean isDebug, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig) {
        startLog(requestName, filePath, isDebug ? Level.DEBUG : Level.INFO, sentryDns, sentryEmail, grayLogConfig);
    }

    public static void startLog(String requestName, String filePath, Level level, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig) {
        startLog(requestName, filePath, level, sentryDns, sentryEmail, grayLogConfig, "", "");
    }
    
    public static void startLog(String requestName, String filePath, Level level, String sentryDns, String sentryEmail, GrayLogConfig grayLogConfig, String currentDate, String currentHour) {
        logLevel = level;
        if (Utils.isEmpty(requestName)) {
            WriteLog.startLog(filePath, level, sentryDns, sentryEmail, grayLogConfig, currentDate, currentHour);
            return;
        }
        
        String pathnameWithTime = filePath;
        File dir = new File(pathnameWithTime);
        if (!dir.exists()) {
            dir.mkdir();
        }
        System.out.println("Current log path: [" + dir + "]");
        ProxyLog log = new ProxyLog(pathnameWithTime, level, requestName, sentryDns, sentryEmail, grayLogConfig, currentDate, currentHour);
        MAP.put(requestName + "_" + currentDate + "_" + currentHour, log);
    }

    private static ProxyLog getLog(String requestName) {
        
        SimpleDateFormat hourSdF = new SimpleDateFormat("HH");
        hourSdF.setTimeZone(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
        String currentHour = hourSdF.format(new Date());
        
        SimpleDateFormat dateSdF = new SimpleDateFormat("yyyyMMdd");
        dateSdF.setTimeZone(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
        String currentDate = dateSdF.format(new Date());
        
        String keyLog = requestName + "_" + currentDate + "_" + currentHour;
        
        if (!MAP.containsKey(keyLog)) {
            startLog(requestName, parentFolder + "/", logLevel, SENTRY_DNS, SENTRY_EMAIL, GRAYLOG_CONFIG, currentDate, currentHour);
        }
        return MAP.get(keyLog);
    }

    public static void debug(String requestName, Object object) {
        if (Utils.isEmpty(requestName)) {
            WriteLog.debug(object);
        } else {
            getLog(requestName).debug(object);
        }
    }

    public static void debug(String requestName, Object transid, Object object) {
        debug(requestName, "[" + transid + "]---" + object);
    }

    public static void debug(String requestName, Object workerName, Object transid, Object object) {
        debug(requestName, "[" + workerName + "]" + "[" + transid + "]", object);
    }

    public static void error(String requestName, Object object, Throwable e) {
        if (Utils.isEmpty(requestName)) {
            WriteLog.error(object, e);
        } else {
            getLog(requestName).error(object, e);
        }
    }

    public static void grayLogInfo(String processName, String jsonData) {
        getLog(processName).sendInfo(processName, jsonData);
    }

    public static void errorSentry(String requestName, Object object, Throwable e) {
        getLog(requestName).sentryError(requestName, object, e);
    }

    public static void error(String requestName, Throwable e) {
        error(requestName, e.getMessage(), e);
    }

    public static void error(String requestName, Object transId, Object object, Throwable e) {
        error(requestName, "[" + transId + "]---" + object, e);
    }

    public static void error(String requestName, Object transId, Object object, String message, Throwable e) {
        error(requestName, String.format("[%s][%s]", transId, object), message, e);
    }

    public static void error(String requestName, Object transid, Object coreTranId, String processName, Object object, Throwable e) {
        error(requestName, String.format("[%s][%s][%s]", transid, coreTranId, processName), object, e);
    }

    public static void write(String requestName, Object object) {
        if (Utils.isEmpty(requestName)) {
            WriteLog.write(object);
        } else {
            getLog(requestName).write(object);
        }
    }

    public static void write(String requestName, Object transid, Object object) {
        write(requestName, Utils.concatString("[", transid, "]---", object));
    }

    public static void write(String requestName, Object requestId, Object processName, Object object) {
        write(requestName, Utils.concatString("[", requestId, "][", processName, "]---", object));
    }

    public static void write(String requestName, Object requestId, Object coreTranId, String processName, Object... objects) {
        write(requestName, Utils.concatString("[", requestId, "][", coreTranId, "][", processName, "]---", objects));
    }

}
