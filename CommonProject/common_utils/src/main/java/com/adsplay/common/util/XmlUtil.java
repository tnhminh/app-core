/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.common.util;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

/**
 *
 * @author khoivu
 */
public class XmlUtil {

    public static String toXml(Object source, Class... type) throws JAXBException {
        StringWriter sw = new StringWriter();
        JAXBContext carContext = JAXBContext.newInstance(type);
        Marshaller carMarshaller = carContext.createMarshaller();
        carMarshaller.marshal(source, sw);
        return sw.toString();
    }

    public static <T extends Object> T toObject(String xml, Class<T> type) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(type);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(xml);
        return (T) unmarshaller.unmarshal(reader);

    }
}
