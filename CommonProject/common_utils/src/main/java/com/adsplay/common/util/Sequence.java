/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.common.util;

import java.util.Arrays;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author khoivu
 */
public class Sequence<T extends Object> {

	private final Queue<T> list = new LinkedBlockingQueue<>();
	private final boolean isSingle;

	public Sequence(T... values) {
		list.addAll(Arrays.asList(values));
		isSingle = values.length <= 1;

	}

	public T next() {
		if (isSingle) {
			return list.peek();
		}
		T poll = list.poll();
		if (poll != null) {
			list.add(poll);
		}
		return poll;
	}

}
