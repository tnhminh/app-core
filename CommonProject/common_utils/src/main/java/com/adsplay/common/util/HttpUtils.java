package com.adsplay.common.util;

import com.adsplay.common.logs.MultiLog;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.SocketConfig;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HTTP;

import javax.ws.rs.core.MediaType;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

/**
 * User: khoivu
 * Date: 3/6/18
 * Time: 8:45 PM
 */
public class HttpUtils {

    public static String send(String processName, String requestId, Object data, String url, Map<String, String> cookies, int timeout) throws Exception {
        MultiLog.write(processName, requestId, "------------------------ SEND HTTP TO PARTNER------------------------------------------------");
        CloseableHttpResponse httpResponse = null;
        try {
            MultiLog.write(processName, requestId, "Data request Partner URL: [" + url + "]");
            MultiLog.write(processName, requestId, "Data request Partner DATA: [" + data + "]");
            HttpPost httpPost = data instanceof String ? getTarget((String) data, url, cookies) : getTarget((List<NameValuePair>) data, url, cookies);
            httpResponse = callPartner(httpPost, processName, requestId, timeout);
            if (null == httpResponse) {
                MultiLog.write(processName, requestId, "Error Partner Response is Null!");
                throw new Exception();
            } else {
                int statusCode = httpResponse.getStatusLine().getStatusCode();
                String content = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
                MultiLog.write(processName, requestId, "PARTNER RESPONSE:[" + content + "]");
                if (HttpURLConnection.HTTP_OK == statusCode) {
                    return content;
                } else {
                    MultiLog.write(processName, requestId, "Error Partner Response StatusCode:" + statusCode);
                    throw new Exception();
                }
            }
        } finally {
            if (null != httpResponse) httpResponse.close();
            MultiLog.write(processName, requestId, "------------------------ SEND HTTP TO PARTNER DONE ------------------------");
        }
    }

    private static HttpPost getTarget(String data, String url, Map<String, String> cookies) {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(data, StandardCharsets.UTF_8));
        httpPost.setHeader(HTTP.CONTENT_TYPE, MediaType.APPLICATION_JSON);
        return httpPost;
    }

    private static HttpPost getTarget(List<NameValuePair> params, String url, Map<String, String> cookies) {
        HttpPost httpPost = new HttpPost(url);
        if (null != cookies) {
            cookies.forEach((k, v) -> {
                httpPost.setHeader("Cookie", k + "=" + v);
            });
        }
        httpPost.setEntity(new UrlEncodedFormEntity(params, StandardCharsets.UTF_8));
        httpPost.setHeader(HTTP.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED);
        return httpPost;
    }

    private static CloseableHttpResponse callPartner(HttpRequestBase httpRequestBase, String processName, String requestId, int timeout) {
        try {
            SocketConfig socketConfig = SocketConfig.custom().setSoTimeout(timeout).build();
            CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultSocketConfig(socketConfig).build();
            return httpClient.execute(httpRequestBase);
        } catch (Exception e) {
            MultiLog.error(processName, requestId, "Error call partner!", e);
            return null;
        }
    }

}
