/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.common.exception;

/**
 *
 * @author khoivu
 */
public class SoapConnectionException extends Throwable {

	public SoapConnectionException() {
	}

	public SoapConnectionException(String message) {
		super(message);
	}

	public SoapConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public SoapConnectionException(Throwable cause) {
		super(cause);
	}

}
