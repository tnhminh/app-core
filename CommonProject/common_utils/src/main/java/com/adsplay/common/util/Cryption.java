package com.adsplay.common.util;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Formatter;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import com.adsplay.common.exception.CryptoException;
import com.adsplay.common.logs.WriteLog;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class Cryption {

    private static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();
    private static final String ENCODING = "UTF-8";

    private static final String HMAC_SHA256 = "HmacSHA256";

    private final static String SECRET_KEY = "6F4B1B252A5F0C3F2992E1A65E56E5B8";

    private static Cipher generateCipher(boolean isEncrypt) throws Exception {
        byte[] raw = SECRET_KEY.getBytes(StandardCharsets.UTF_8);
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        if (isEncrypt) cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        else cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        return cipher;
    }

    public static String decrypt(String str) throws Exception {
        // Decode base64 to get bytes
        byte[] dec = new BASE64Decoder().decodeBuffer(str);
        // Decrypt
        byte[] utf8 = generateCipher(false).doFinal(dec);
        // Decode using utf-8
        return new String(utf8, StandardCharsets.UTF_8);
    }

    public static String encrypt(String str) throws Exception {
        // Encode the string into bytes using utf-8
        byte[] utf8 = str.getBytes("UTF8");

        // Encrypt
        byte[] enc = generateCipher(true).doFinal(utf8);

        // Encode bytes to base64 to get a string
        return new BASE64Encoder().encode(enc);
    }

    public static String signHmacSHA256(String data, String secretKey) throws Exception {
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256);
        Mac mac = Mac.getInstance(HMAC_SHA256);
        mac.init(secretKeySpec);
        byte[] rawHmac = mac.doFinal(data.getBytes("UTF-8"));
        return DatatypeConverter.printBase64Binary(rawHmac).replace("\n", "");
    }

    public static String getSHA(String data) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(data.getBytes("UTF-8"));
        byte[] ba = md.digest();
        StringBuilder sb = new StringBuilder(ba.length * 2);

        for (int i = 0; i < ba.length; i++) {
            sb.append(HEX_CHARS[(((int) ba[i] & 0xFF) / 16) & 0x0F]);
            sb.append(HEX_CHARS[((int) ba[i] & 0xFF) % 16]);
        }
        return sb.toString();
    }

    public static String hashSHA(String input) throws CryptoException {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA");
            sha.update(input.getBytes());
            BigInteger dis = new BigInteger(1, sha.digest());
            String result = dis.toString(16);
            return result.toUpperCase();
        } catch (NoSuchAlgorithmException ex) {
            throw new CryptoException(ex);
        }
    }

    public static String hashSHA256(String input) throws CryptoException {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-256");
            sha.update(input.getBytes());
            BigInteger dis = new BigInteger(1, sha.digest());
            String result = dis.toString(16);
            if (!result.startsWith("0") && result.length() < 64) {
                result = "0" + result;
            }
            return result.toUpperCase();
        } catch (NoSuchAlgorithmException ex) {
            throw new CryptoException(ex);
        }
    }

    public static String hmacSha1(String value, String key) throws CryptoException {
        try {
            // Get an hmac_sha1 key from the raw key bytes
            byte[] keyBytes = key.getBytes();
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);

            // Compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(value.getBytes());
            return Base64.encodeBase64String(rawHmac);
        } catch (Exception e) {
            throw new CryptoException(e);
        }
    }

    public static String hmacSha1ToHex(String value, String key) throws CryptoException {
        return hmacSha1ToHex(value.getBytes(), key);
    }

    public static String hmacSha1ToHex(byte[] value, String key) throws CryptoException {
        try {
            // Get an hmac_sha1 key from the raw key bytes
            byte[] keyBytes = key.getBytes();
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);

            // Compute the hmac on input data bytes
            return toHexString(mac.doFinal(value));
        } catch (Exception e) {
            throw new CryptoException(e);
        }
    }

    public static String decryptRSA(String encryptData, String privateKey) throws CryptoException {
        try {
            byte[] privateKeyBytes = Base64.decodeBase64(privateKey);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            PrivateKey prvk = keyFactory.generatePrivate(privateKeySpec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, prvk);
            return new String(cipher.doFinal(Base64.decodeBase64(encryptData)));
        } catch (Exception ex) {
            throw new CryptoException(ex);
        }
    }

    public static String encryptRSA(byte[] inpBytes, String publicKey) throws CryptoException {
        try {
            byte[] publicKeyBytes = Base64.decodeBase64(publicKey);
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey pubk = keyFactory.generatePublic(publicKeySpec);
            return encryptRSA(inpBytes, pubk);
        } catch (Exception ex) {
            throw new CryptoException(ex);
        }
    }

    private static String encryptRSA(byte[] inpBytes, PublicKey pubk) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pubk);
        return Base64.encodeBase64String(cipher.doFinal(inpBytes)).replace("\r", "");
    }

    public static String md5(String content) throws CryptoException {
        try {
            byte[] bytes = content.getBytes(ENCODING);
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(bytes);
            byte[] output = md.digest();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < output.length; i++) {
                builder.append(Integer.toString((output[i] & 0xff) + 0x100, 16).substring(1));
            }
            return builder.toString();

        } catch (Exception ex) {
            throw new CryptoException(ex);
        }
    }

    public static String signData(String toBeSigned, String privateKey) throws CryptoException {
        return signData(toBeSigned, privateKey.getBytes());
    }

    public static String signData(String toBeSigned, byte[] privateKey) throws CryptoException {
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKey);
            PrivateKey prvk = keyFactory.generatePrivate(privateKeySpec);
            return signDataBase64(toBeSigned, prvk);
        } catch (Exception e) {
            throw new CryptoException(e);
        }
    }

    public static String signDataBase64(String content, String prk) throws CryptoException {
        try {
            byte[] privateKeyBytes = Base64.decodeBase64(prk);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            PrivateKey prvk = keyFactory.generatePrivate(privateKeySpec);
            return signDataBase64(content, prvk);
        } catch (Exception ex) {
            throw new CryptoException(ex);
        }
    }

    public static String signDataBase64(String content, PrivateKey prk) throws CryptoException {
        try {
            Signature rsa = Signature.getInstance("SHA1withRSA");
            rsa.initSign(prk);
            rsa.update(content.getBytes());
            return new String(Base64.encodeBase64(rsa.sign()));
        } catch (Exception ex) {
            throw new CryptoException(ex);
        }
    }

    public static boolean verifySignature(byte[] signature, byte[] toBeSign, String plk) {
        try {
            PublicKey pubk;
            byte[] publicKeyBytes = Base64.decodeBase64(plk);
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            pubk = keyFactory.generatePublic(publicKeySpec);
            Signature sign = Signature.getInstance("SHA1withRSA");
            sign.initVerify(pubk);
            sign.update(toBeSign);
            return sign.verify(signature);
        } catch (Exception e) {
            WriteLog.error("VERIFY SIGNATURE DATA - EXCEPTION:", e);
        }
        return false;
    }

    public static boolean verifySignatureBase64(String signature, String toBeSign,
                                                String plk) throws CryptoException {
        try {
            byte[] publicKeyBytes = Base64.decodeBase64(plk);
            EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey pubk = keyFactory.generatePublic(publicKeySpec);
            return verifySignatureBase64(signature, toBeSign, pubk);
        } catch (Exception e) {
            throw new CryptoException(e);
        }
    }

    public static boolean verifySignatureBase64(String signature,
                                                String toBeSign, PublicKey pubk) throws CryptoException {
        try {
            Signature sign = Signature.getInstance("SHA1withRSA");
            byte[] ssignature = Base64.decodeBase64(signature);
            sign.initVerify(pubk);
            sign.update(toBeSign.getBytes());
            return sign.verify(ssignature);
        } catch (Exception e) {
            throw new CryptoException(e);
        }
    }

    private static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);

        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        return sb.toString();
    }

    public static void main(String[] args) throws IOException, CryptoException, NoSuchAlgorithmException {
        try {
            String adsplay = encrypt("campaign2019-01-012019-01-17");
            System.out.println(adsplay);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static byte[] readBytes(String path) throws IOException {
        return Files.readAllBytes(Paths.get(path));
    }

    private static String readFile(String path) throws IOException {
        StringBuilder sb = new StringBuilder();
        Files.lines(Paths.get(path), StandardCharsets.UTF_8).forEach(data -> {
            sb.append(data);
        });
        return sb.toString();
    }

    public static void createRSAKey(String publicKeyFile, String privateKeyFile) throws NoSuchAlgorithmException, IOException {
        File privateKey = new File(privateKeyFile);
        if (!privateKey.exists()) {
            privateKey.createNewFile();
        }
        File publicKey = new File(privateKeyFile);
        if (!publicKey.exists()) {
            publicKey.createNewFile();
        }
        createRSAKey(publicKey, privateKey);
    }

    public static void createRSAKey(File publicKeyFile, File privateKeyFile) throws NoSuchAlgorithmException, IOException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(1024);
        KeyPair keyPair = generator.generateKeyPair();
        byte[] privateKey = keyPair.getPrivate().getEncoded();
        String base64PrivateKey = Base64.encodeBase64String(privateKey);
        FileUtils.writeStringToFile(privateKeyFile, base64PrivateKey, "UTF-8");

        byte[] publicKey = keyPair.getPublic().getEncoded();
        String base64PublicKey = Base64.encodeBase64String(publicKey);
        FileUtils.writeStringToFile(publicKeyFile, base64PublicKey, "UTF-8");
    }

    public static PublicKey createPublicKey(String modulus, String exponent) throws InvalidKeySpecException, NoSuchAlgorithmException {
        byte[] modBytes = Base64.decodeBase64(modulus);
        byte[] expBytes = Base64.decodeBase64(exponent);
        BigInteger modBig = new BigInteger(1, modBytes);
        BigInteger expBig = new BigInteger(1, expBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec aPublicKeySpec = new RSAPublicKeySpec(modBig, expBig);
        return keyFactory.generatePublic(aPublicKeySpec);
    }

    public static PrivateKey createPrivateKey(String modulus, String db) throws InvalidKeySpecException, NoSuchAlgorithmException {
        byte[] modlBytes = Base64.decodeBase64(modulus);
        byte[] dBytes = Base64.decodeBase64(db);
        BigInteger modBig = new BigInteger(1, modlBytes);
        BigInteger dBig = new BigInteger(1, dBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(modBig, dBig);
        return keyFactory.generatePrivate(rsaPrivateKeySpec);
    }

}
