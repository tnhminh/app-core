/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.crypto;

import com.adsplay.common.exception.CryptoException;
import org.bouncycastle.openpgp.*;

import java.io.*;
import java.security.NoSuchProviderException;
import java.util.Iterator;

/**
 * @author khoivu
 */
public class PGPChecker {

    private static final int BUFFER_SIZE = 1 << 16; // should always be power of 2(one shifted bitwise 16 places)

    public static void main(String[] args) throws IOException, PGPException, NoSuchProviderException, CryptoException {
        PGPHelper.init(
                "/Users/macos/Dropbox/Public/My Documents/gateway/facebook/fb_api/key/live/fb_pub.asc",
                "/Users/macos/Dropbox/Public/My Documents/gateway/facebook/fb_api/key/live/momo-sec.asc",
                "ms3rvice",
                -3377610467294118039l,
                9039551695284096153l);

        String descryptData = "-----BEGIN PGP MESSAGE-----\n"
                + "Version: GnuPG v2.0.22 (GNU/Linux)\n"
                + "\n"
                + "hQEMA/WKF/zsGWHWAQf/UIG4qghtOIXaKpNNjMe4p+6qqxaHd3b0wqrrykqnYn5p\n"
                + "V/bC+XJUMjzv4dKTgXiNe+QTZ7XIM5+1IAVsoblkQHA7FRaFnrbZ4fkr9oc0F1Kk\n"
                + "imH66C5zSch43+NyxPNRNmDQAuTAo75284uMc/99X0RpYRJ4wERxh4vCFzYknAH9\n"
                + "WEa60x8LgZBj1bxwsg20QAAZk11vGzYAP/FAxwnWka6FWZKEQj+LCFPsyMHRBS9x\n"
                + "uPW4R5jEQtfh4gDWbEZp0lQzFH2Is3PA8fkdarxz2kF4G5BG+MwOnHdWNeNHAMGY\n"
                + "Dick8sSymfZ2vCB4soiwDs5RfDKmmcfL05/pezxMf9LpAcTKnXi17zixdjmjzpC2\n"
                + "Kk9AjFUJZX4yZPkH0ozPCEe2h9U6UTySnx25rqKdm6rAgYIafBEf4J2khGJkKRgI\n"
                + "2kgE6m6m6A7AheaMJ7ATatbmYuCZ5cKn+zfzh60oGTsrq/w9KRsKSR88dX8OHQpg\n"
                + "jkxCkti5mJ5sIbEOsR/e2X8hu9QzIxgnkBo0kqQPhZalOx+PY1fhFQ5EmxbA4qKW\n"
                + "heyWsjNj1Nc+2xAED7KgcpRAhREonP9IO5oVpo258Luos6VETubuW3G2DMI+rt4i\n"
                + "3q0Ozqj6GxcSDAathVbhtOFiF2Y8ZS1L5ffW0YXnRlmGhrZe1lqUXgpuit3HtaZc\n"
                + "UXwd4BSqC39UZFxalElqeTSrOHkFbLUylc8YITNffkwbPaAQb+5wtDlSXNrv8r+X\n"
                + "GxWHFiQlLZst76koQTjw1gHH3subLs/eUu4FUTYOrxkCtQWg6W0qpX3eYakguVWA\n"
                + "WgVKNU/0/2CQ+tD3hJ43wO8+RZh2K+60cacJNpwGjC1UE/UsIdEWz121de/bbXME\n"
                + "TqEL1ZhL7ewcj2cvynnBLirW0TwjKNftI8mZEFq5Qlg6ramj4QxoEgoOCiIvXrjO\n"
                + "lp1VyPUCK8s/GinHlSAcObOPXuEvTShxNWdwuMiZinhKy1jja4l9x1vjeKb2r/H4\n"
                + "Cru3axYe7pKq30iSzyWYO6B1DD5W1GxiAnVUK/37udROGxhmuZnWDKC5OJVXK+Af\n"
                + "lhcftOLFy7CmbMme2bCnADJTQMRjR++xfRUcrlT0NNgZYa/zR8P0yc8FJ6mY6l5x\n"
                + "SrQBLElp0FURvCZ4a6L0qeUeRMA1M8B2QeNUYH3zTJDACopMPAWy\n"
                + "=yLbK\n"
                + "-----END PGP MESSAGE-----";

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PGPHelper.getInstance().decryptAndVerifySignature(descryptData.getBytes(), baos);
        System.out.println(new String(baos.toByteArray()));

        InputStream pubStream = new FileInputStream(new File("/Users/macos/Dropbox/Public/My Documents/gateway/facebook/fb_api/key/live/fb_pub.asc"));
        byte[] data = "Hello World".getBytes();
        pubStream = PGPUtil.getDecoderStream(pubStream);
        PGPPublicKeyRingCollection pkCol = new PGPPublicKeyRingCollection(pubStream);
        PGPPublicKeyRing pkRing;
        Iterator it = pkCol.getKeyRings();
        int ring = 0;
        while (it.hasNext()) {
            ring++;
            pkRing = (PGPPublicKeyRing) it.next();
            Iterator pkIt = pkRing.getPublicKeys();
            int keySize = 0;
            while (pkIt.hasNext()) {
                keySize++;
                PGPPublicKey key = (PGPPublicKey) pkIt.next();

                if (key.isEncryptionKey()) {
                    System.out.println(ring + "/" + keySize + ": EncryptionKey:" + key.getKeyID());
                    PGPHelper.getInstance().encryptAndSign(data, baos);
                    System.out.println(ring + "/" + keySize + ": Encryption Data:" + new String(baos.toByteArray()));
                }
                if (key.isMasterKey()) {
                    System.out.println(ring + "/" + keySize + ": MasterKey");
                }
                if (key.isRevoked()) {
                    System.out.println(ring + "/" + keySize + ": Revoked");
                } else {
                    System.out.println(ring + "/" + keySize + ": Unkown:" + key.getKeyID());
                }
            }
        }

    }
}
