package com.adsplay.crypto;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Base64;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */

/**
 * @author khoivu
 */
public class AESCrypto {

    public static String encrypt(String plainText, String secretKey, String iv)
            throws Exception {
        return encrypt(plainText, createSecretKey(secretKey), new IvParameterSpec(iv.getBytes()));
    }

    public static String encrypt(String plainText, SecretKey secretKey, IvParameterSpec ivParameterSpec)
            throws Exception {

        byte[] plainTextByte = plainText.getBytes("UTF-8");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec);
        byte[] encryptedByte = cipher.doFinal(plainTextByte);
        Base64.Encoder encoder = Base64.getEncoder();
        String encryptedText = encoder.encodeToString(encryptedByte);
        return encryptedText;
    }

    public static SecretKey createSecretKey(String secretKey) throws Exception {
        byte[] key = secretKey.getBytes("UTF-8");
        MessageDigest sha = MessageDigest.getInstance("SHA-256");
        key = sha.digest(key);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

    public static String decrypt(String encryptedText, String secretKey, String iv)
            throws Exception {
        return decrypt(encryptedText, createSecretKey(secretKey), new IvParameterSpec(iv.getBytes()));
    }

    public static String decrypt(String encryptedText, SecretKey secretKey, IvParameterSpec ivParameterSpec)
            throws Exception {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] encryptedTextByte = decoder.decode(encryptedText);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ivParameterSpec);
        byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
        String decryptedText = new String(decryptedByte);
        return decryptedText;
    }
}
