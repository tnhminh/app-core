package com.adsplay.crypto;

import com.adsplay.common.exception.CryptoException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.Arrays;

/**
 * @author khoivu
 */
public class TripleDesCrypto {

    private static final String TRIPLE_DES_TRANSFORMATION = "DESede/ECB/PKCS7Padding";
    private static final String ALGORITHM = "DESede";
    private static final String BOUNCY_CASTLE_PROVIDER = "BC";
    private static final String UNICODE_FORMAT = "UTF-8";
    public static final String HASH_ALGORITHM = "SHA";

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    /*
     * To do : encrypt plaintext using 3Des algorithm
     */
    private static byte[] encode(byte[] input, String key) throws Exception {
        Cipher encrypter = Cipher.getInstance(TRIPLE_DES_TRANSFORMATION, BOUNCY_CASTLE_PROVIDER);
        //hash key to sha, and init encrypter
        encrypter.init(Cipher.ENCRYPT_MODE, buildKey(key.toCharArray()));
        //encrypt
        return encrypter.doFinal(input);
    }

    /*
     * To do : decrypt plaintext using 3Des algorithm
     */
    private static byte[] decode(byte[] input, String key) throws CryptoException {
        try {
            Cipher decrypter = Cipher.getInstance(TRIPLE_DES_TRANSFORMATION, BOUNCY_CASTLE_PROVIDER);
            //hash key to sha, and init decrypter
            decrypter.init(Cipher.DECRYPT_MODE, buildKey(key.toCharArray()));
            //decrypt
            return decrypter.doFinal(input);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException | UnsupportedEncodingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            throw new CryptoException(ex.getMessage(), ex);
        }
    }

    /*
     * to do : string to byte , UTF-8 format
     */
    private static byte[] getByte(String string) throws Exception {
        return string.getBytes(UNICODE_FORMAT);
    }

    /*
     * to do : byte to String
     */
    private static String getString(byte[] byteText) {
        return new String(byteText);
    }

    /*
     * generate has key using SHA
     */
    private static Key buildKey(char[] password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest digester = MessageDigest.getInstance(HASH_ALGORITHM);
        digester.update(String.valueOf(password).getBytes(UNICODE_FORMAT));
        byte[] key = digester.digest();

        //3des key using 24 byte, convert to 24 byte
        byte[] keyDes = Arrays.copyOf(key, 24);
        return new SecretKeySpec(keyDes, ALGORITHM);
    }

    /*
     * encrypt using 3 des
     */
    public static String encrypt(String plainText, String key) throws Exception {
        byte[] encryptedByte = encode(getByte(plainText), key);
        return Hex.encodeHexString(encryptedByte);
    }

    /*
     * decrypt using 3 des
     */
    public static String decrypt(String cipherText, String key) throws CryptoException {
        byte[] decryptedByte;
        try {
            decryptedByte = decode(Hex.decodeHex(cipherText.toCharArray()), key);
            return getString(decryptedByte);
        } catch (DecoderException ex) {
            throw new CryptoException(ex.getMessage(), ex);
        }
    }

    /*
     * generate has key using SHA
     */
    public String generateSHA(String password) throws Exception {
        MessageDigest digester = MessageDigest.getInstance(HASH_ALGORITHM);
        digester.update(String.valueOf(password.toCharArray()).getBytes(UNICODE_FORMAT));
        byte[] key = digester.digest();
        return Hex.encodeHexString(key);
    }
}
