#!/bin/bash

export JAVA_HOME=/usr/bin/java
PACKET="insert_wifi_isc-1.0-SNAPSHOT.jar"
APPNAME="insert_wifi_isc"

LOG_FILE="./log/report.nohup"
CORECONF="./conf/core_config.json"
GATEWAYCONF="./conf/gateway.json"
DATACONF="./conf/data_manager.json"

# JVM
XMS=1g
XMX=2g
MaxPermSize=128m
PermSize=64M
JVM_OPTS="-XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintStringDeduplicationStatistics  -Xms$XMS  -Xmx$XMX -XX:PermSize=$PermSize -XX:MaxPermSize=$MaxPermSize -Djava.net.preferIPv4Stack=true -XX:+HeapDumpOnOutOfMemoryError -XX:+UseThreadPriorities"

JAVA_OPTS="-Dfile.encoding=UTF-8 $JVM_OPTS -jar $PACKET -conf $CORECONF -gatewayConf $GATEWAYCONF -dataManagerConf $DATACONF"

checkpid()
{
        echo $(ps -ef | grep java | grep $APPNAME | grep -v grep | awk '{ print $2}')
}

start ()
{
        if [ $(checkpid) ] ; then
                echo -e "\n$APPNAME is running (pid:$(checkpid))\n"
        else
                echo ""
                printf "$APPNAME is starting..."

                nohup $JAVA_HOME ${JAVA_OPTS} > $LOG_FILE 2>&1 &
                echo "done!"
        fi

}

stop ()
{
        if [ $(checkpid) ] ; then
                kill -9 $(checkpid)
                echo -e "\n$APPNAME stop success\n"
        fi

}

status ()
{
        if [ $(checkpid) ] ; then
                echo -e "\n$APPNAME is running (pid:$(checkpid))\n"
        else
#!/bin/bash

export JAVA_HOME=/usr/bin/java
PACKET="insert_wifi_isc-1.0-SNAPSHOT.jar"
APPNAME="insert_wifi_isc"

LOG_FILE="./log/report.nohup"
CORECONF="./conf/core_config.json"
GATEWAYCONF="./conf/gateway.json"
DATACONF="./conf/data_manager.json"

# JVM
XMS=1g
XMX=2g
MaxPermSize=128m
PermSize=64M
JVM_OPTS="-XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+PrintStringDeduplicationStatistics  -Xms$XMS  -Xmx$XMX -XX:PermSize=$PermSize -XX:MaxPermSize=$MaxPermSize -Djava.net.preferIPv4Stack=true -XX:+HeapDumpOnOutOfMemoryError -XX:+UseThreadPriorities"

JAVA_OPTS="-Dfile.encoding=UTF-8 $JVM_OPTS -jar $PACKET -conf $CORECONF -gatewayConf $GATEWAYCONF -dataManagerConf $DATACONF"

checkpid()
{
        echo $(ps -ef | grep java | grep $APPNAME | grep -v grep | awk '{ print $2}')
}

start ()
{
        if [ $(checkpid) ] ; then
                echo -e "\n$APPNAME is running (pid:$(checkpid))\n"
        else
                echo ""
                printf "$APPNAME is starting..."

                nohup $JAVA_HOME ${JAVA_OPTS} > $LOG_FILE 2>&1 &
                echo "done!"
        fi

}

stop ()
{
        if [ $(checkpid) ] ; then
                kill -9 $(checkpid)
                echo -e "\n$APPNAME stop success\n"
        fi

}

status ()
{
        if [ $(checkpid) ] ; then
                echo -e "\n$APPNAME is running (pid:$(checkpid))\n"
        else
