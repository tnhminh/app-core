package com.adsplay.delivery.process;

import com.adsplay.conf.GatewayConf;
import com.adsplay.service.Init;
import com.adsplay.delivery.model.config.ModuleConf;
import com.adsplay.delivery.model.config.QueryConf;
import org.apache.commons.io.FileUtils;

import java.io.File;

/**
 * User: khoivu
 * Date: 9/27/18
 * Time: 2:45 PM
 */
public class InitProcess implements Init {
    @Override
    public void init(GatewayConf config) throws Exception {
        File fileModule = new File(config.getModuleConfPath());
        String modulePath = FileUtils.readFileToString(fileModule, "UTF-8");
        ModuleConf.init(modulePath);
    }
}