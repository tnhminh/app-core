package com.adsplay.delivery.process;

import java.text.SimpleDateFormat;

import com.adsplay.conf.Configuration;
import com.adsplay.conf.CoreConfig;
import com.adsplay.delivery.model.DefaultRequest;
import com.adsplay.delivery.model.DefaultResponse;
import com.adsplay.delivery.model.config.ModuleConf;
import com.adsplay.delivery.model.config.QueryConf;
import com.adsplay.gateway.model.DefaultCode;
import com.adsplay.gateway.model.GwCode;
import com.adsplay.gateway.model.GwException;
import com.adsplay.model.Service;
import com.adsplay.process.AbstractProcess;

/**
 * User: khoivu
 * Date: 24/01/19
 * Time: 15:40
 * @param <F>
 */
public abstract class BaseProcess<T extends DefaultRequest, R extends DefaultResponse<F>, H, F> extends AbstractProcess<T, R, H> {

    protected CoreConfig coreConfig;

    // Redis handler here

    public BaseProcess(String processName) {
        super(processName);
    }

    @Override
    protected void validate(Service service, T request) throws GwException {
        //NOOP
    }

    @Override
    protected void baseValidation(String requestId, String reportType, T request) throws GwException {
        //NOOP
    }

    protected abstract F dataResponse(Service service, String requestId, T request, R response, String requestData) throws GwException, Exception;

    @Override
    protected void perform(String requestId, Service service, T request, R response, String requestData) throws GwException, Exception {
        F result = dataResponse(service, requestId, request, response, requestData);
        if (null != result) {
            response.setData(result);
        }
        response.setErrorCode(DefaultCode.SUCCESS);
        finishProcess(service, request, response);
    }

    @Override
    protected GwCode getInternalError() {
        return DefaultCode.OTHER_ERROR;
    }
}
