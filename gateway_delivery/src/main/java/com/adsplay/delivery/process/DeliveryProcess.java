package com.adsplay.delivery.process;

import java.io.IOException;

import com.adsplay.delivery.model.DefaultRequest;
import com.adsplay.delivery.model.DefaultResponse;
import com.adsplay.delivery.output.OutputFactory;
import com.adsplay.gateway.model.GwException;
import com.adsplay.model.Service;

public class DeliveryProcess<T extends DefaultRequest, R extends DefaultResponse<F>, H, F> extends BaseProcess<T, R, H, F> {

    public DeliveryProcess(String processName) {
        super(processName);
    }

    @Override
    protected F dataResponse(Service service, String requestId, T request, R response, String requestData) throws GwException, Exception {
        // Do response here
        return null;
    }

    @Override
    protected T createRequest(String request) throws IOException {
        return null;
    }

    @Override
    public R createResponse() {
        return null;
    }

    @Override
    public void setHandler(H handler) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public OutputFactory createOutputFactory() throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

}
