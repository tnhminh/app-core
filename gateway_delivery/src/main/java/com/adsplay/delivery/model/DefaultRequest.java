package com.adsplay.delivery.model;

import com.adsplay.gateway.model.Request;

public class DefaultRequest extends Request {

    private String uid = "";

    private long cb = 0L;

    private String out = "";

    private String ip = "";

    private int scw = 0;

    private int sch = 0;

    private String ext = "";
    
    private String ua = "";
    
    public String getUuid() {
        return uid;
    }

    public void setUuid(String uuid) {
        this.uid = uuid;
    }

    public long getCacheBuster() {
        return cb;
    }

    public void setCacheBuster(long cacheBuster) {
        this.cb = cacheBuster;
    }

    public void setOutput(String output) {
        this.out = output;
    }
    
    @Override
    public String getOutputType() {
        return out;
    }

    public String getIpClient() {
        return ip;
    }

    public void setIpClient(String ipClient) {
        this.ip = ipClient;
    }

    public int getScreenWidth() {
        return scw;
    }

    public void setScreenWidth(int screenWidth) {
        this.scw = screenWidth;
    }

    public int getScreenHeight() {
        return sch;
    }

    public void setScreenHeight(int screenHeight) {
        this.sch = screenHeight;
    }

    public String getExtraData() {
        return ext;
    }

    public void setExtraData(String extraData) {
        this.ext = extraData;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }
}