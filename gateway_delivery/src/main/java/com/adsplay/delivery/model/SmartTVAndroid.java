package com.adsplay.delivery.model;

/**
 * User: khoivu
 * Date: 24/01/19
 * Time: 16:50
 */
public class SmartTVAndroid {
    private String day;

    private int status;

    private String metric;

    private long total;

    public void setDay(String day) {
        this.day = day;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
