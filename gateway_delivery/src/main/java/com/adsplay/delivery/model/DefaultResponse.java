package com.adsplay.delivery.model;

import com.adsplay.gateway.model.GwCode;
import com.adsplay.gateway.model.Response;

import java.util.HashMap;
import java.util.List;

/**
 * User: khoivu. Updated by Minh To Ran FollowingFirstParam: 8/27/18 Time: 2:29
 * PM
 */
public class DefaultResponse<T> extends Response<T> {

    private int statusCode;

    private String message;

    private String referenceId;

    private String contentType = "";

    private HashMap<String, T> data = new HashMap<>();

    @Override
    public void setStatus(int status) {
        this.statusCode = status;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @Override
    public void setErrorCode(GwCode code) {
        this.statusCode = code.getCode();
        this.message = code.getMessage();
    }

    public int getStatus() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public String getReferenceId() {
        return referenceId;
    }

    @Override
    public T getData(String key) {
        return getData().get(key);
    }

    public HashMap<String, T> getData() {
        if (null == data) {
            data = new HashMap<>();
        }
        return data;
    }

    public void setData(List<T> data) {
        for (T object : data) {
            String fieldName = object.getClass().getSimpleName();
            getData().put(lowerCaseFirstCharacter(fieldName), object);
        }
    }

    public void setData(T object) {
        String fieldName = lowerCaseFirstCharacter(object.getClass().getSimpleName());
        if (null == getData(fieldName)) {
            getData().put(fieldName, object);
        }
    }

    public void setData(String fieldName, T object) {
        fieldName = lowerCaseFirstCharacter(fieldName);
        if (null == getData(fieldName)) {
            getData().put(fieldName, object);
        }
    }

    // lowerCase first Character
    protected String lowerCaseFirstCharacter(String className) {
        if (className.length() > 1) {
            Character firstChar = className.charAt(0);
            return firstChar.toString().toLowerCase() + className.subSequence(1, className.length());
        } else {
            return className.toLowerCase();
        }
    }

    public String getContentType() {
        return contentType;
    }

    @Override
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
