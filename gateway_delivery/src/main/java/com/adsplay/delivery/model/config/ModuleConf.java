package com.adsplay.delivery.model.config;

import com.adsplay.conf.JsonUtil;

/**
 * User: khoivu
 * Date: 4/27/18
 * Time: 9:36 AM
 */
public class ModuleConf {

    private transient static ModuleConf queryConf;

    private String insertSurvey;

    public static void init(String jsonModuleConfig) throws Exception {
        queryConf = JsonUtil.fromString(ModuleConf.class, jsonModuleConfig);
    }

    public static ModuleConf getInstance() {
        if (queryConf == null) {
            throw new NullPointerException("Please invoke the Init method FIRST. ONLY ONE TIME!");
        }
        return queryConf;
    }

    public String getInsertSurvey() {
        return insertSurvey;
    }
}
