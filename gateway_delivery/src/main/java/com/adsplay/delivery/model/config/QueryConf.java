package com.adsplay.delivery.model.config;

import com.adsplay.conf.JsonUtil;

/**
 * User: khoivu
 * Date: 4/27/18
 * Time: 9:36 AM
 */
public class QueryConf {

    private transient static QueryConf queryConf;

    private String reportSmartTVByDay;

    public static void init(String jsonModuleConfig) throws Exception {
        queryConf = JsonUtil.fromString(QueryConf.class, jsonModuleConfig);
    }

    public static QueryConf getInstance() {
        if (queryConf == null) {
            throw new NullPointerException("Please invoke the Init method FIRST. ONLY ONE TIME!");
        }
        return queryConf;
    }

    public String getReportSmartTVByDay() {
        return reportSmartTVByDay;
    }
}
