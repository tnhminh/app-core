package com.adsplay.delivery.database;

public class RepositoryFactory {

    private static RepositoryFactory inst;

    private RepositoryFactory() {
    }

    public static RepositoryFactory getInst() {
        if (null == inst) {
            synchronized (RepositoryFactory.class) {
                inst = new RepositoryFactory();
            }
        }
        return inst;
    }

    public void init() {

    }

}
