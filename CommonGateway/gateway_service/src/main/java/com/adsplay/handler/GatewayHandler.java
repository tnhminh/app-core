/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.handler;

import com.adsplay.model.Service;

/**
 * @author khoivu
 */
public interface GatewayHandler<T> {

    void handle(String traceId, String process, Service service, T data);

}
