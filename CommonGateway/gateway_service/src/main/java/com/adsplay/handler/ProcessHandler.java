/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.handler;

import com.adsplay.gateway.model.Request;
import com.adsplay.gateway.model.Response;
import com.adsplay.model.Service;

/**
 * @author khoivu
 */
public interface ProcessHandler<T extends Request, R extends Response> {

    public void handle(Service service, T request, R data);
}
