package com.adsplay.delivery.output;

import java.util.List;

import com.adsplay.gateway.model.Response;

/**
 * 
 * @author Minh Tran
 *
 */
public abstract class BaseOutput<T> implements OutputProcess<T> {

    private static final String FIELD_DEFAULT = "fullLogAd";

    protected boolean isEmptyData(Object data) {
        if (data == null) {
            return false;
        } else if (data instanceof List<?>) {
            return ((List<?>) data).isEmpty();
        } else if (data instanceof String) {
            return ((String) data).isEmpty();
        }
        return false;
    }

    protected abstract String generateOutput(T dataResponse);

    @Override
    public String export(Response<T> dataResponse) {
        if (isEmptyData(dataResponse)) {
            return "";
        }
        return generateOutput(dataResponse.getData(FIELD_DEFAULT));
    }
}
