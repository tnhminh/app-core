package com.adsplay.delivery.output;

import com.adsplay.delivery.utils.ContentTypeUtils;
import com.adsplay.gateway.model.Response;

/**
 * 
 * @author Minh Tran
 *
 */
public abstract class OutputFactory {

    public static final String VAST = "vast";

    public static final String JSON = "json";

    public static final String HTML = "html";

    public OutputFactory() {

    }

    /**
     * Get Output Handler by using output type
     * 
     * @param outputType
     * @return
     */
    public OutputProcess getOutputManagement(String outputType, Response response) {
        // Set content type for response
        response.setContentType(ContentTypeUtils.getContentType(outputType));
        if (outputType == null) {
            return new BaseOutput() {

                @Override
                protected String generateOutput(Object dataResponse) {
                    // TODO Auto-generated method stub
                    return null;
                }
            };
        }

        return selectOutput(outputType);
    }

    /**
     * Do select output type
     * @param outputType
     * @return
     */
    abstract protected OutputProcess selectOutput(String outputType);
        
}
