package com.adsplay.delivery.utils;

import java.util.HashMap;
import java.util.Map;

public class ContentTypeUtils {

    private static final Map<String, String> OUTPUT_CONTENT_TYPE_MAPPING = new HashMap<String, String>();
    static {
        OUTPUT_CONTENT_TYPE_MAPPING.put("html", "text/html");
        OUTPUT_CONTENT_TYPE_MAPPING.put("vast", "application/xml");
        OUTPUT_CONTENT_TYPE_MAPPING.put("json", "application/json");
    }

    public static String getContentType(String outputType) {
        String contentType = OUTPUT_CONTENT_TYPE_MAPPING.get(outputType);
        return contentType!=null && !contentType.isEmpty() ? contentType : "";
    }
}
