package com.adsplay.delivery.output;

import com.adsplay.gateway.model.Response;

public interface OutputProcess<T> {

    String export(Response<T> dataResponse);

}
