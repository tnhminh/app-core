package com.adsplay.service;

import com.adsplay.conf.GatewayConf;

/**
 * User: khoivu
 * Date: 8/26/18
 * Time: 8:27 PM
 */
public interface Init {

    public void init(GatewayConf config) throws Exception;

}
