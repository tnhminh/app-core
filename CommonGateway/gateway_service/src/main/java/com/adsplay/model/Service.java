package com.adsplay.model;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.common.logs.WriteLog;
import com.adsplay.delivery.output.OutputFactory;
import com.adsplay.gateway.model.Request;
import com.adsplay.gateway.model.Response;
import com.adsplay.handler.GatewayHandler;
import com.jamonapi.MonitorFactory;

public class Service extends AbstractService {

    private OutputFactory outFactory;

    public Service(String processName, GatewayHandler<String> rootHandler) {
        this(processName);
        this.rootHandlers.add(rootHandler);
    }

    public OutputFactory getOutFactory() {
        return outFactory;
    }

    public void setOutFactory(OutputFactory outFactory) {
        this.outFactory = outFactory;
    }

    public Service(String processName) {
        monitor = MonitorFactory.start(processName);
    }

    @Override
    protected String buildingResult(Response response, Request request) {
        try {
            String outType = request.getOutputType();
            return outFactory.getOutputManagement(outType, response).export(response);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return requestId;
    }

    public void finish(String requestName, String processName, Response response, Request request) {
        try {
            this.response = response;
            
            monitor.stop();
            singleMonitor.stop();
            request.setMonitorRawLog(singleMonitor.toString());
            
            MultiLog.write(prefix.replace("/", "_"), request.getProcessName(), request.getRequestId(), "Log-Detail :[" + request.toString() + "]");

            finish(requestName, processName, buildingResult(response, request));
        } catch (Exception e) {
            finish(requestName, processName, response.toString());
            MultiLog.error("error", "Log-Detail :[" + request.toString() + "]", e);
        }
    }

    @Override
    public void finish(String requestName, String processName, String response) {
        if (replied) {
            MultiLog.write(requestName, requestId, processName, "Message has replied. DO NOTHING!!");
            return;
        }
        replied = true;
        rootHandlers.forEach(entry -> entry.handle(requestId, processName, this, response));

    }
}
