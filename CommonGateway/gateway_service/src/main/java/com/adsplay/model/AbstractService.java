package com.adsplay.model;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

import com.adsplay.gateway.model.Request;
import com.adsplay.gateway.model.Response;
import com.adsplay.handler.GatewayHandler;
import com.adsplay.handler.ProcessHandler;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

public abstract class AbstractService {

    protected String requestId;

    protected String type;
    
    protected String prefix;

    protected Response response;

    protected Monitor monitor;
    
    protected Monitor singleMonitor;

    protected boolean replied = false;

    protected ProcessHandler processHandler;

    protected final Queue<GatewayHandler<String>> rootHandlers = new LinkedBlockingDeque<>();

    abstract String buildingResult(Response response, Request request);

    public void finish(String requestName, String processName, Response response, Request request) {
        try {
            this.response = response;
            finish(requestName, processName, buildingResult(response, request));
        } catch (Exception e) {
            finish(requestName, processName, response.toString());
        }
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    abstract void finish(String requestName, String processName, String response);

    public void setProcessHandler(ProcessHandler processHandler) {
        this.processHandler = processHandler;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        singleMonitor = MonitorFactory.start(requestId);
        this.requestId = requestId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public void setMonitor(Monitor monitor) {
        this.monitor = monitor;
    }

    public Monitor getSingleMonitor() {
        return singleMonitor;
    }

    public void setSingleMonitor(Monitor singleMonitor) {
        this.singleMonitor = singleMonitor;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
