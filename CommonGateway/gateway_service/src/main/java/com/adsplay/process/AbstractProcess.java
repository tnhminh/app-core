package com.adsplay.process;

import com.adsplay.common.logs.MultiLog;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.Configuration;
import com.adsplay.conf.GatewayConf;
import com.adsplay.delivery.output.OutputFactory;
import com.adsplay.gateway.model.GwCode;
import com.adsplay.gateway.model.GwException;
import com.adsplay.gateway.model.Request;
import com.adsplay.gateway.model.Response;
import com.adsplay.handler.ProcessHandler;
import com.adsplay.model.Service;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author khoivu
 * @param <F>
 */
public abstract class AbstractProcess<T extends Request, R extends Response, H> implements GatewayProcess<R, H>, ProcessHandler<T, R> {

    protected final GatewayConf config;

    protected ExecutorService executorService;

    private final String processName;

    protected String type;

    protected String requestId;

    public AbstractProcess(String processName) {
        this.processName = processName;
        this.config = Configuration.getInstance().getGatewayConf();
        this.executorService = Executors.newWorkStealingPool();
    }

    protected final String processName() {
        return processName;
    }

    protected abstract T createRequest(String request) throws IOException;

    protected abstract void perform(String requestId, Service service, T request, R response, String requestData) throws GwException, Exception;

    protected abstract void validate(Service service, T request) throws GwException;
    
    @Override
    public void run(Service service, String requestData) throws Exception {
        T request = createRequest(requestData);
        requestId = service.getRequestId();
        type = service.getType();
        request.setType(type);
        request.setRequestId(requestId);
        request.setProcessName(processName());
        //writeInfo(request, "[RECEIVED REQUEST]", requestData);
        //TODO store graylog
        //        MultiLog.grayLogInfo(processName(), requestData);
        executorService.submit(() -> {
            try {
                baseValidation(requestId, type, request);
                writeInfo(request, "Validate....");
                validate(service, request);
                writeInfo(request, "Validate....DONE");
                writeInfo(request, "PERFORM");
                service.setProcessHandler(this);
                R response = createResponse();
                response.setReferenceId(createReferenceId());
                perform(requestId, service, request, response, requestData);
            } catch (GwException ex) {
                doWithError(request, service, ex);
            } catch (Throwable ex) {
                writeError(request, ex);
                doWithError(request, service, getInternalError());
            }
        });
    }

    protected abstract GwCode getInternalError();

    private void doWithError(T request, Service tran, GwException ex) {
        doWithError(request, tran, ex.getCode());
    }

    private void doWithError(T request, Service service, GwCode errorCode) {
        writeInfo(request, "Error that handle, ErrorCode:[" + errorCode + "]");
        R gatewayResponse = createResponse();
        gatewayResponse.setErrorCode(errorCode);
        gatewayResponse.setReferenceId(createReferenceId());
        doWithError(service, request, gatewayResponse);
    }

    protected void doWithError(Service tran, T request, R gatewayResponse) {
        finishProcess(tran, request, gatewayResponse);
    }

    protected void finishProcess(Service service, T request, R gatewayResponse) {
        writeInfo(request, "Process....DONE");
        service.finish(type, processName, gatewayResponse, request);
    }

    protected abstract void baseValidation(String requestId, String reportType, T request) throws GwException;

    protected String createReferenceId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void handle(Service service, T request, R data) {
        finishProcess(service, request, data);
    }

    public void writeError(Request request, Throwable e) {
        MultiLog.error(Utils.nullToEmpty(request.getProcessName()),
                       Utils.nullToEmpty(request.getRequestId()),
                       e);
    }

    public void writeInfo(Request request, Object... objects) {
        MultiLog.write(Utils.nullToEmpty(request.getProcessName()),
                       Utils.nullToEmpty(request.getRequestId()),
                       objects);
    }

}
