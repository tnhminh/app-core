/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.process;

import java.io.IOException;

import com.adsplay.delivery.output.OutputFactory;
import com.adsplay.gateway.model.Response;
import com.adsplay.model.Service;

/**
 * @author khoivu
 */
public interface GatewayProcess<R extends Response, H> {

    public void run(Service service, String body) throws Exception;

    public R createResponse();
    
    void setHandler(H handler);
    
    OutputFactory createOutputFactory() throws IOException;
}
