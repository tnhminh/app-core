package com.adsplay.gateway;

import com.adsplay.common.logs.WriteLog;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.Configuration;
import com.adsplay.conf.GatewayConf;
import com.jamonapi.MonitorComposite;
import com.jamonapi.MonitorFactory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;

import java.util.Date;

/**
 * User: khoivu
 * Date: 12/7/18
 * Time: 11:00 AM
 */
public class HttpServer extends AbstractVerticle {

    @Override
    public void start() throws Exception {

        WriteLog.write("START", "Start external api...[" + deploymentID() + "]");
        GatewayConf config = Configuration.getInstance().getGatewayConf();
        Router router = Router.router(vertx);
        RouterFactory.getInstance().addExternal(router);
        HttpServerOptions options = new HttpServerOptions();
        options.setCompressionSupported(true);
        options.setCompressionLevel(3);

        router.route("/admin/*").handler((routingContext) -> {
            try {
                String pass = routingContext.request().headers().get("password");
                if (!Utils.nullToEmpty(pass).equals("taothich")) {
                    throw new Exception();
                }
                routingContext.next();
            } catch (Exception ex) {
                WriteLog.error("MONITOR", "Error monitor: ", ex);
                routingContext.response()
                              .setStatusCode(406)
                              .end("UnSupport");
            }
        });

        router.get("/admin/monitor")
              .handler(routingContext -> {
                  String response;
                  try {
                      MonitorComposite composite = MonitorFactory.getComposite("AllMonitors");
                      String[] header = composite.getDisplayHeader();
                      Object[][] data = composite.getDisplayData();
                      int rows = data.length;
                      int col = header.length;
                      JsonArray array = new JsonArray();
                      for (int i = 0; i < rows; i++) {
                          JsonObject object = new JsonObject();
                          Object[] detail = data[i];
                          int actualCol = Math.min(detail.length, col);
                          for (int j = 0; j < actualCol; j++) {
                              object.put(header[j], parseData(detail[j]));
                          }
                          array.add(object);
                      }
                      response = new JsonObject().put("data", array).encode();

                  } catch (Exception ex) {
                      WriteLog.error("RELOAD", "Reload Error: ", ex);
                      response = "Reload Error: " + Utils.exceptionToString(ex);
                  }
                  routingContext.response()
                                .putHeader("content-type", "application/jon").
                                        setChunked(true)
                                .write(response)
                                .end();
              });
        vertx.createHttpServer(options).requestHandler(router::accept).listen(config.getPort());
        WriteLog.write("START API DONE!!!  PORT[" + config.getPort() + "]");
    }

    private Object parseData(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Date) {
            return ((Date) object).toString();
        }
        return object;
    }
}
