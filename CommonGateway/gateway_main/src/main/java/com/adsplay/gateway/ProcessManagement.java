/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.adsplay.gateway;

import com.adsplay.Constant;
import com.adsplay.common.logs.MultiLog;
import com.adsplay.common.logs.WriteLog;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.JsonUtil;
import com.adsplay.entity.GwApi;
import com.adsplay.gateway.model.DefaultCode;
import com.adsplay.gateway.model.GwCode;
import com.adsplay.gateway.model.Response;
import com.adsplay.model.Service;
import com.adsplay.process.GatewayProcess;
import com.adsplay.service.DynamicClassFactory;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author khoivu
 */
public class ProcessManagement {

    private static final ProcessManagement INSTANCE = new ProcessManagement();

    private final Map<String, GatewayProcess> processes = new ConcurrentHashMap<>();

    public static ProcessManagement getInstance() {
        return INSTANCE;
    }

    private ProcessManagement() {

    }

    private GatewayProcess getProcess(GwApi restApi) {
        if (!processes.containsKey(restApi.getName())) {
            GatewayProcess gatewayProcess = DynamicClassFactory.getInstance().createObject(
                    restApi.getExecuteClass(),
                    GatewayProcess.class,
                    restApi.getName()
            );
            processes.put(restApi.getName(), gatewayProcess);
            return gatewayProcess;
        }
        return processes.get(restApi.getName());
    }

    public void process(GwApi restApi, RoutingContext routingContext) {
        GatewayProcess process = getProcess(restApi);
        execute(restApi.getName(), restApi, routingContext, process);
    }

    private void execute(String processName, GwApi restApi, RoutingContext context, GatewayProcess gatewayProcess) {
        HttpServerRequest httpRequest = context.request();
        String type;
        String jsonData = "";
        if (restApi.isHeader()) {
            type = Utils.nullToEmpty(httpRequest.getHeader(Constant.TYPE));
        } else {
            MultiMap params = httpRequest.params();
            params.set(Constant.USER_AGENT, Utils.nullToEmpty(httpRequest.getHeader(Constant.USER_AGENT)));
            jsonData = createData(params);
            type = Utils.nullToEmpty(params.get(Constant.TYPE));
        }

        //TODO:
        /*if (Utils.isEmpty(type)) {
            reject(type, processName, "Invalid request name. Return STATUS:" + 406, httpRequest);
            return;
        }*/

        if (HttpMethod.POST.name().equals(restApi.getMethod())) {
            httpRequest.bodyHandler((Buffer event) -> {
                String body = event.toString("UTF-8");
                try {
                    WriteLog.write(type, processName, "START", "*********************************************************************************");
                    MultiLog.write(type, processName, "RECEIVED REQUEST", "Data: [" + body + "]");
                    perform(type, restApi, context, gatewayProcess, processName, body);
                } catch (DecodeException ex) {
                    WriteLog.error(processName, "Error message: [" + ex.getMessage() + "]", ex);
                    doWithError(type, processName, restApi, gatewayProcess.createResponse(), context, DefaultCode.INVALID_JSON_STRING);
                } catch (Exception ex) {
                    WriteLog.error(processName, "Error message: [" + ex.getMessage() + "]", ex);
                    doWithError(type, processName, restApi, gatewayProcess.createResponse(), context, DefaultCode.OTHER_ERROR);
                }
            });
        } else {
            try {
                MultiLog.write(type, processName, "START", "*********************************************************************************");
                if (restApi.isHeader()) {
                    MultiMap params = httpRequest.headers();
                    jsonData = createData(params);
                }
                MultiLog.write(type, processName, "RECEIVED REQUEST", "param data: " + jsonData);
                perform(type, restApi, context, gatewayProcess, processName, jsonData);

            } catch (Exception ex) {
                WriteLog.error(type, processName, "Error message: [" + ex.getMessage() + "]", ex);
                doWithError(type, processName, restApi, gatewayProcess.createResponse(), context, DefaultCode.OTHER_ERROR);
            }
        }
    }

    private void perform(String type, GwApi restApi, RoutingContext context, GatewayProcess gatewayProcess, String processName, String message) throws Exception {
        Service service = new Service(processName, (String traceId, String process, Service s, String response) -> reply(type, traceId, processName, restApi, context, response));
        service.setRequestId(System.currentTimeMillis() + "");
        service.setType(type);
        gatewayProcess.run(service, message);
    }

    protected void reply(String type, String requestId, String processName, GwApi api, RoutingContext context, String data) {
        WriteLog.write(type, processName, requestId, "REPLY RESPONSE:  RAW DATA:[" + data + "]");
        context.response()
               .putHeader("content-type", api.getContentType())
               .putHeader("accept", api.getAccepted())
               .setChunked(true)
               .write(data)
               .end();
    }

    private void doWithError(String type, String processName, GwApi restApi, Response response, RoutingContext context, GwCode ex) {
        MultiLog.write(type, processName, "Error that handle, ErrorCode:[" + ex.getCode() + "], message: [" + ex.getMessage() + "]");
        response.setErrorCode(ex);
        response.setReferenceId(UUID.randomUUID().toString());
        try {
            reply(type, processName, processName, restApi, context, JsonUtil.toString(response));
        } catch (Exception e) {
            reply(type, processName, processName, restApi, context, response.toString());
        }
    }

    private void reject(String type, String processName, String message, HttpServerRequest httpRequest) {
        MultiLog.write(type, processName, "RECEIVED REQUEST", message);
        httpRequest.response()
                   .setStatusCode(406)
                   .end("UnSupport");
    }

    private String createData(MultiMap params) {
        if (params.isEmpty()) {
            return "";
        }
        JsonObject jsonObject = new JsonObject();
        params.entries().forEach(entry -> jsonObject.put(entry.getKey(), entry.getValue()));

        return jsonObject.encode();

    }

}
