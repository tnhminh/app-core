/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.adsplay.gateway;

import com.adsplay.common.logs.WriteLog;
import com.adsplay.database.DataRepository;
import com.adsplay.entity.GwApi;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author khoivu
 */
public class RouterFactory extends AbstractVerticle {

    private static final RouterFactory INSTANCE = new RouterFactory();

    private final List<Router> external;

    private final List<Router> admin;

    private final List<GwApi> apis;

    public RouterFactory() {
        external = new ArrayList<>();
        admin = new ArrayList<>();
        apis = new ArrayList<>();
    }

    public static RouterFactory getInstance() {
        return INSTANCE;
    }

    public void addAdmin(Router admin) {
        this.admin.add(admin);
        loadApi(admin, "ADMIN");
    }

    public void addExternal(Router external) {
        this.external.add(external);
        loadApi(external, "EXTERNAL");
    }

    private void loadApi(Router router, String type) {
        apis.stream().filter((restApi) -> (type.equalsIgnoreCase(restApi.getType()))).forEachOrdered((restApi) -> loadApi(router, restApi));
    }

    private void addApiProcess(String moduleName, GwApi restApi, List<Router> routers) {
        routers.forEach(entry -> loadApi(entry, restApi));
    }

    private void loadApi(Router router, GwApi restApi) {
        WriteLog.write("START", "Register API Path :[" + restApi.getPath() + "], Method: [" + restApi.getMethod() + "]");
        router.route(HttpMethod.valueOf(restApi.getMethod()), restApi.getPath())
              .consumes(restApi.getContentType())
              .produces(restApi.getAccepted())
              .handler(routingContext -> ProcessManagement.getInstance().process(restApi, routingContext));
    }

    public void loadResApi(String projectName) {
        List<GwApi> restApis = DataRepository.getRestApis(
                projectName,
                UUID.randomUUID().toString(),
                projectName
        );
        this.apis.addAll(restApis);
    }

}
