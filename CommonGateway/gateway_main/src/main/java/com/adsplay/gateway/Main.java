package com.adsplay.gateway;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

import com.adsplay.common.logs.WriteLog;
import com.adsplay.common.util.UserAgentUtils;
import com.adsplay.common.util.Utils;
import com.adsplay.conf.Configuration;

import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

/**
 * @author khoivu
 */
public class Main {

    public static void main(String[] args) throws Exception {
        
    }

    private static void deployAPI(Vertx vertx, String verticleName, int instances, int poolSize) {
        WriteLog.write("START", "Deploy verticle:[" + verticleName + "]");
        DeploymentOptions gatewayOptions = new DeploymentOptions()
                .setWorker(true)
                .setWorkerPoolName("GW")
                .setInstances(instances)
                .setWorkerPoolSize(poolSize);
        vertx.deployVerticle(verticleName, gatewayOptions, (AsyncResult<String> event) -> {
            if (event.succeeded()) {
                WriteLog.write("START", "Deploy [" + verticleName + "successfully...] " + event.result());
            } else {
                WriteLog.write("START", "Deploy [" + verticleName + "],FAIL...:");
                if (event.cause() != null) {
                    event.cause().printStackTrace();
                    System.exit(0);
                }
            }
        });
    }

    private static String[]  configPaths = new String[] {"-conf", "./conf/core_config.json", "-gatewayConf", "./conf/gateway.json", "-dataManagerConf", "./conf/data_manager.json"};
    private static void loadConfig(String[] args) throws IOException, Exception {
        String pathGatewayConfig = null;
        String pathCoreConfig = null;
        String pathDataManagerConfig = null;
        args = configPaths;
        
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            if ("-conf".equalsIgnoreCase(arg) && !Utils.isEmpty(args[i + 1])) {
                pathCoreConfig = args[i + 1];
            }

            if ("-gatewayConf".equalsIgnoreCase(arg) && !Utils.isEmpty(args[i + 1])) {
                pathGatewayConfig = args[i + 1];
            }

            if ("-dataManagerConf".equalsIgnoreCase(arg) && !Utils.isEmpty(args[i + 1])) {
                pathDataManagerConfig = args[i + 1];
            }
        }
        System.out.println("Config folder path:[" + pathCoreConfig + "]");
        System.out.println("DataManager folder path:[" + pathDataManagerConfig + "]");
        System.out.println("Gateway config path:[" + pathGatewayConfig + "]");

        if (Utils.isEmpty(pathGatewayConfig)
            || Utils.isEmpty(pathCoreConfig)
            || Utils.isEmpty(pathDataManagerConfig)) {
            System.out.println("************************************************************************************");
            System.out.println("*                                                                                  *");
            System.out.println("*  Vui long set them thong so tao folder 'conf' va dua tat ca file config vao      *");
            System.out.println("*  File config phai co ten theo dinh dang  fb_xxxx.json                            *");
            System.out.println("*  Them thong so:                                                                  *");
            System.out.println("*       -coreConf        [Path]      :    Duong dan toi file cau hinh chung        *");
            System.out.println("*       -gatewayConf     [Path]      :    Duong dan file cau hinh                  *");
            System.out.println("*                                                                                  *");
            System.out.println("*                                                                                  *");
            System.out.println("************************************************************************************");
            System.exit(0);
        }

        String coreConfig = FileUtils.readFileToString(new File(pathCoreConfig), StandardCharsets.UTF_8);
        String gatewayConfig = FileUtils.readFileToString(new File(pathGatewayConfig), StandardCharsets.UTF_8);
        String dataManagerConfig = FileUtils.readFileToString(new File(pathDataManagerConfig), StandardCharsets.UTF_8);
        Configuration.init(coreConfig, gatewayConfig, dataManagerConfig);
        UserAgentUtils.init();
    }

}
