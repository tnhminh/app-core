package com.adsplay.gateway.model;

/**
 * User: khoivu
 * Date: 8/27/18
 * Time: 10:02 AM
 */
public class GwException extends Throwable {

    private final GwCode errorCode;

    public GwException(GwCode errorCode) {
        this.errorCode = errorCode;
    }

    public GwException(GwCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public GwException(GwCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public GwException(GwCode errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    public GwCode getCode() {
        return errorCode;
    }
}
