package com.adsplay.gateway.model;

/**
 * User: khoivu
 * Date: 8/27/18
 * Time: 10:03 AM
 */
public interface GwCode {
    int getCode();

    String getMessage();
}
