/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsplay.gateway.model;

/**
 *
 * @author khoivu
 */
public enum DefaultCode implements GwCode {
    SUCCESS                                         (200, "Success"),
    INVALID_DATA                                    (101, "Invalid data"),
    INVALID_REPORT_TYPE                             (102,"Invalid report type"),
    INVALID_AD_TYPE                                 (103, "Invalid ad_type param"),
    INVALID_ID                                      (104,"Invalid id param"),
    INVALID_DATE                                    (105,"Invalid date param"),
    INVALID_FOLLOWING                               (106,"Invalid following param"),
    INVALID_INCLUDES                                (107,"Invalid includes param"),
    INVALID_PLATFORM                                (108,"Invalid platform param"),
    INVALID_JSON_STRING                             (109,"Invalid Json String"),
    REPORT_NOT_FOUND                                (404, "Report not found"),
    OTHER_ERROR                                     (6969,"Other Error"),
	;

    private final int code;
    private final String message;

    private DefaultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public static GwCode findByCode(int code) {
        for (DefaultCode errorCode : values()) {
            if (errorCode.getCode() == code) {
                return errorCode;
            }
        }
        return DefaultCode.OTHER_ERROR;
    }

}
