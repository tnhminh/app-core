package com.adsplay.gateway.model;

import java.io.IOException;

import com.adsplay.conf.JsonUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jamonapi.Monitor;

/**
 * User: khoivu Date: 8/26/18 Time: 11:59 PM
 */
public abstract class Request {

    @JsonProperty("ads-type")
    private String type;

    private String requestId;
    private String processName;
    
    private String monitorRawLog;

    @JsonIgnore
    public String getProcessName() {
        return processName;
    }

    @JsonIgnore
    public void setProcessName(String processName) {
        this.processName = processName;
    }

    @JsonIgnore
    public String getRequestId() {
        return requestId;
    }

    @JsonIgnore
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public abstract String getOutputType();
    
    public String getMonitorRawLog() {
        return monitorRawLog;
    }

    public void setMonitorRawLog(String monitorRawLog) {
        this.monitorRawLog = monitorRawLog;
    }

    @Override
    public String toString() {
        try {
            return JsonUtil.toString(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
