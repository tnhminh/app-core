package com.adsplay.gateway.model;

public abstract class Response<T> {

    public abstract void setStatus(int status);

    public abstract void setMessage(String message);

    public abstract void setReferenceId(String referenceId);

    public abstract void setErrorCode(GwCode code);

    public abstract void setContentType(String contentType);
    
    public abstract String getContentType();

    public abstract T getData(String fieldName);
}
